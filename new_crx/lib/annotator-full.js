!function() {
  var $;
  var Annotator;
  var Delegator;
  var LinkParser;
  var Range;
  var Util;
  var base64Decode;
  var base64UrlDecode;
  var createDateFromISO8601;
  var findChild;
  var fn;
  var functions;
  var g;
  var getNodeName;
  var getNodePosition;
  var gettext;
  var parseToken;
  var simpleXPathJQuery;
  var simpleXPathPure;
  var _Annotator;
  var _gettext;
  var _j;
  var _i;
  var _len1;
  var _len;
  var _ref;
  var _ref1;
  var _ref2;
  var _ref3;
  var _ref4;
  var _t;
  /** @type {function (this:(Array.<T>|string|{length: number}), *=, *=): Array.<T>} */
  var __slice = [].slice;
  /** @type {function (this:Object, *): boolean} */
  var __hasProp = {}.hasOwnProperty;
  /**
   * @param {Function} child
   * @param {Object} parent
   * @return {?}
   */
  var __extends = function(child, parent) {
    /**
     * @return {undefined}
     */
    function ctor() {
      /** @type {Function} */
      this.constructor = child;
    }
    var key;
    for (key in parent) {
      if (__hasProp.call(parent, key)) {
        child[key] = parent[key];
      }
    }
    ctor.prototype = parent.prototype;
    child.prototype = new ctor;
    child.__super__ = parent.prototype;
    return child;
  };
  /**
   * @param {?} fn
   * @param {?} me
   * @return {?}
   */
  var __bind = function(fn, me) {
    return function() {
      return fn.apply(me, arguments);
    };
  };
  /** @type {function (this:(Array.<T>|string|{length: number}), T, number=): number} */
  var __indexOf = [].indexOf || function(item) {
    /** @type {number} */
    var i = 0;
    var l = this.length;
    for (;i < l;i++) {
      if (i in this && this[i] === item) {
        return i;
      }
    }
    return-1;
  };
  /**
   * @param {string} context
   * @return {?}
   */
  simpleXPathJQuery = function(context) {
    var jq;
    jq = this.map(function() {
      var node;
      var value;
      var expires;
      var sel;
      /** @type {string} */
      expires = "";
      node = this;
      for (;(node != null ? node.nodeType : void 0) === Node.ELEMENT_NODE && node !== context;) {
        sel = node.tagName.replace(":", "\\:");
        value = $(node.parentNode).children(sel).index(node) + 1;
        /** @type {string} */
        value = "[" + value + "]";
        /** @type {string} */
        expires = "/" + node.tagName.toLowerCase() + value + expires;
        node = node.parentNode;
      }
      return expires;
    });
    return jq.get();
  };
  /**
   * @param {Object} step
   * @return {?}
   */
  simpleXPathPure = function(step) {
    var getPathSegment;
    var getPathTo;
    var jq;
    var n;
    /**
     * @param {string} node
     * @return {?}
     */
    getPathSegment = function(node) {
      var name;
      var pos;
      name = getNodeName(node);
      pos = getNodePosition(node);
      return "" + name + "[" + pos + "]";
    };
    /** @type {Object} */
    n = step;
    /**
     * @param {Object} node
     * @return {?}
     */
    getPathTo = function(node) {
      var xpath;
      /** @type {string} */
      xpath = "";
      for (;node !== n;) {
        if (node == null) {
          throw new Error("Called getPathTo on a node which was not a descendant of @rootNode. " + n);
        }
        /** @type {string} */
        xpath = getPathSegment(node) + "/" + xpath;
        node = node.parentNode;
      }
      /** @type {string} */
      xpath = "/" + xpath;
      /** @type {string} */
      xpath = xpath.replace(/\/$/, "");
      return xpath;
    };
    jq = this.map(function() {
      var path;
      path = getPathTo(this);
      return path;
    });
    return jq.get();
  };
  /**
   * @param {Element} node
   * @param {HTMLElement} element
   * @param {string} value
   * @return {?}
   */
  findChild = function(node, element, value) {
    var child;
    var _ref;
    var item;
    var name;
    var _i;
    var _len;
    if (!node.hasChildNodes()) {
      throw new Error("XPath error: node has no children!");
    }
    _ref = node.childNodes;
    /** @type {number} */
    item = 0;
    /** @type {number} */
    _i = 0;
    _len = _ref.length;
    for (;_i < _len;_i++) {
      child = _ref[_i];
      name = getNodeName(child);
      if (name === element) {
        item += 1;
        if (item === value) {
          return child;
        }
      }
    }
    throw new Error("XPath error: wanted child not found.");
  };
  /**
   * @param {Node} node
   * @return {?}
   */
  getNodeName = function(node) {
    var nodeName;
    nodeName = node.nodeName.toLowerCase();
    switch(nodeName) {
      case "#text":
        return "text()";
      case "#comment":
        return "comment()";
      case "#cdata-section":
        return "cdata-section()";
      default:
        return nodeName;
    }
  };
  /**
   * @param {?} node
   * @return {?}
   */
  getNodePosition = function(node) {
    var pos;
    var tmp;
    /** @type {number} */
    pos = 0;
    tmp = node;
    for (;tmp;) {
      if (tmp.nodeName === node.nodeName) {
        pos++;
      }
      tmp = tmp.previousSibling;
    }
    return pos;
  };
  /** @type {null} */
  gettext = null;
  if (typeof Gettext !== "undefined" && Gettext !== null) {
    _gettext = new Gettext({
      domain : "annotator"
    });
    /**
     * @param {string} msgid
     * @return {?}
     */
    gettext = function(msgid) {
      return _gettext.gettext(msgid);
    };
  } else {
    /**
     * @param {string} msgid
     * @return {?}
     */
    gettext = function(msgid) {
      return msgid;
    };
  }
  /**
   * @param {string} msgid
   * @return {?}
   */
  _t = function(msgid) {
    return gettext(msgid);
  };
  // console.log(_ref);
  if (!(typeof jQuery !== "undefined" && jQuery !== null ? (_ref = jQuery.fn) != null ? _ref.jquery : void 0 : void 0)) {
    console.error(_t("Annotator requires jQuery: have you included lib/vendor/jquery.js?"));
  }
  if (!(JSON && (JSON.parse && JSON.stringify))) {
    console.error(_t("Annotator requires a JSON implementation: have you included lib/vendor/json2.js?"));
  }
  $ = jQuery;
  Util = {};
  /**
   * @param {Object} array
   * @return {?}
   */
  Util.flatten = function(array) {
    var flatten;
    /**
     * @param {Array} array
     * @return {?}
     */
    flatten = function(array) {
      var el;
      var flat;
      var _i;
      var _len;
      /** @type {Array} */
      flat = [];
      /** @type {number} */
      _i = 0;
      _len = array.length;
      for (;_i < _len;_i++) {
        el = array[_i];
        /** @type {Array} */
        flat = flat.concat(el && $.isArray(el) ? flatten(el) : el);
      }
      return flat;
    };
    return flatten(array);
  };
  /**
   * @param {?} arr
   * @param {Object} ss
   * @return {?}
   */
  Util.contains = function(arr, ss) {
    var tapElement;
    /** @type {Object} */
    tapElement = ss;
    for (;tapElement != null;) {
      if (tapElement === arr) {
        return true;
      }
      tapElement = tapElement.parentNode;
    }
    return false;
  };
  /**
   * @param {Array} jq
   * @return {?}
   */
  Util.getTextNodes = function(jq) {
    var getTextNodes;
    /**
     * @param {Node} node
     * @return {?}
     */
    getTextNodes = function(node) {
      var nodes;
      if (node && node.nodeType !== Node.TEXT_NODE) {
        /** @type {Array} */
        nodes = [];
        if (node.nodeType !== Node.COMMENT_NODE) {
          node = node.lastChild;
          for (;node;) {
            nodes.push(getTextNodes(node));
            node = node.previousSibling;
          }
        }
        return nodes.reverse();
      } else {
        return node;
      }
    };
    return jq.map(function() {
      return Util.flatten(getTextNodes(this));
    });
  };
  /**
   * @param {Node} node
   * @return {?}
   */
  Util.getLastTextNodeUpTo = function(node) {
    var getLastTextNodeUpTo;
    switch(node.nodeType) {
      case Node.TEXT_NODE:
        return node;
      case Node.ELEMENT_NODE:
        if (node.lastChild != null) {
          getLastTextNodeUpTo = Util.getLastTextNodeUpTo(node.lastChild);
          if (getLastTextNodeUpTo != null) {
            return getLastTextNodeUpTo;
          }
        }
        break;
    }
    node = node.previousSibling;
    if (node != null) {
      return Util.getLastTextNodeUpTo(node);
    } else {
      return null;
    }
  };
  /**
   * @param {number} el
   * @return {?}
   */
  Util.getFirstTextNodeNotBefore = function(el) {
    var getFirstTextNodeNotBefore;
    switch(el.nodeType) {
      case Node.TEXT_NODE:
        return el;
      case Node.ELEMENT_NODE:
        if (el.firstChild != null) {
          getFirstTextNodeNotBefore = Util.getFirstTextNodeNotBefore(el.firstChild);
          if (getFirstTextNodeNotBefore != null) {
            return getFirstTextNodeNotBefore;
          }
        }
        break;
    }
    el = el.nextSibling;
    if (el != null) {
      return Util.getFirstTextNodeNotBefore(el);
    } else {
      return null;
    }
  };
  /**
   * @param {?} r
   * @return {?}
   */
  Util.readRangeViaSelection = function(r) {
    var selection;
    selection = Util.getGlobal().getSelection();
    selection.removeAllRanges();
    selection.addRange(r.toRange());
    return selection.toString();
  };
  /**
   * @param {?} el
   * @param {Object} relativeRoot
   * @return {?}
   */
  Util.xpathFromNode = function(el, relativeRoot) {
    var bulk;
    var result;
    try {
      result = simpleXPathJQuery.call(el, relativeRoot);
    } catch (fn) {
      bulk = fn;
      console.log("jQuery-based XPath construction failed! Falling back to manual.");
      result = simpleXPathPure.call(el, relativeRoot);
    }
    return result;
  };
  /**
   * @param {string} xp
   * @param {(Function|string)} root
   * @return {?}
   */
  Util.nodeFromXPath = function(xp, root) {
    var idx;
    var value;
    var node;
    var part;
    var parts;
    var j;
    var subLn;
    var _ref1;
    parts = xp.substring(1).split("/");
    /** @type {(Function|string)} */
    node = root;
    /** @type {number} */
    j = 0;
    subLn = parts.length;
    for (;j < subLn;j++) {
      part = parts[j];
      _ref1 = part.split("[");
      value = _ref1[0];
      idx = _ref1[1];
      /** @type {number} */
      idx = idx != null ? parseInt((idx != null ? idx.split("]") : void 0)[0]) : 1;
      node = findChild(node, value.toLowerCase(), idx);
    }
    return node;
  };
  /**
   * @param {string} str
   * @return {?}
   */
  Util.escape = function(str) {
    return str.replace(/&(?!\w+;)/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;");
  };
  Util.uuid = function() {
    var counter;
    /** @type {number} */
    counter = 0;
    return function() {
      return counter++;
    };
  }();
  /**
   * @return {?}
   */
  Util.getGlobal = function() {
    return function() {
      return this;
    }();
  };
  /**
   * @param {Array} scripts
   * @return {?}
   */
  Util.maxZIndex = function(scripts) {
    var applyArgs;
    var node;
    applyArgs = function() {
      var _i;
      var _len;
      var _results;
      /** @type {Array} */
      _results = [];
      /** @type {number} */
      _i = 0;
      _len = scripts.length;
      for (;_i < _len;_i++) {
        node = scripts[_i];
        if ($(node).css("position") === "static") {
          _results.push(-1);
        } else {
          _results.push(parseFloat($(node).css("z-index")) || -1);
        }
      }
      return _results;
    }();
    return Math.max.apply(Math, applyArgs);
  };
  /**
   * @param {Object} event
   * @param {Text} element
   * @return {?}
   */
  Util.mousePosition = function(event, element) {
    var nodeOfs;
    var currentElementPosition;
    if ((currentElementPosition = $(element).css("position")) !== "absolute" && (currentElementPosition !== "fixed" && currentElementPosition !== "relative")) {
      element = $(element).offsetParent()[0];
    }
    nodeOfs = $(element).offset();
    return{
      top : event.pageY - nodeOfs.top,
      left : event.pageX - nodeOfs.left
    };
  };
  /**
   * @param {Function} types
   * @return {?}
   */
  Util.preventEventDefault = function(types) {
    return types != null ? typeof types.preventDefault === "function" ? types.preventDefault() : void 0 : void 0;
  };
  /** @type {Array} */
  functions = ["log", "debug", "info", "warn", "exception", "assert", "dir", "dirxml", "trace", "group", "groupEnd", "groupCollapsed", "time", "timeEnd", "profile", "profileEnd", "count", "clear", "table", "error", "notifyFirebug", "firebug", "userObjects"];
  if (typeof console !== "undefined" && console !== null) {
    if (console.group == null) {
      /**
       * @param {?} name
       * @return {?}
       */
      console.group = function(name) {
        return console.log("GROUP: ", name);
      };
    }
    if (console.groupCollapsed == null) {
      /** @type {Function} */
      console.groupCollapsed = console.group;
    }
    /** @type {number} */
    _j = 0;
    /** @type {number} */
    _len1 = functions.length;
    for (;_j < _len1;_j++) {
      fn = functions[_j];
      if (console[fn] == null) {
        /**
         * @return {?}
         */
        console[fn] = function() {
          return console.log(_t("Not implemented:") + (" console." + name));
        };
      }
    }
  } else {
    this.console = {};
    /** @type {number} */
    _i = 0;
    /** @type {number} */
    _len = functions.length;
    for (;_i < _len;_i++) {
      fn = functions[_i];
      /**
       * @return {undefined}
       */
      this.console[fn] = function() {
      };
    }
    /**
     * @return {?}
     */
    this.console["error"] = function() {
      var dig;
      /** @type {Array.<?>} */
      dig = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      return alert("ERROR: " + dig.join(", "));
    };
    /**
     * @return {?}
     */
    this.console["warn"] = function() {
      var dig;
      /** @type {Array.<?>} */
      dig = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      return alert("WARNING: " + dig.join(", "));
    };
  }
  Delegator = function() {
    /**
     * @param {?} element
     * @param {?} options
     * @return {undefined}
     */
    function Delegator(element, options) {
      this.options = $.extend(true, {}, this.options, options);
      this.element = $(element);
      this._closures = {};
      this.on = this.subscribe;
      this.addEvents();
    }
    Delegator.prototype.events = {};
    Delegator.prototype.options = {};
    /** @type {null} */
    Delegator.prototype.element = null;
    /**
     * @return {?}
     */
    Delegator.prototype.destroy = function() {
      return this.removeEvents();
    };
    /**
     * @return {?}
     */
    Delegator.prototype.addEvents = function() {
      var event;
      var _i;
      var _len;
      var _ref;
      var _results;
      _ref = Delegator._parseEvents(this.events);
      /** @type {Array} */
      _results = [];
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        event = _ref[_i];
        _results.push(this._addEvent(event.selector, event.event, event.functionName));
      }
      return _results;
    };
    /**
     * @return {?}
     */
    Delegator.prototype.removeEvents = function() {
      var event;
      var _i;
      var _len;
      var _ref;
      var _results;
      _ref = Delegator._parseEvents(this.events);
      /** @type {Array} */
      _results = [];
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        event = _ref[_i];
        _results.push(this._removeEvent(event.selector, event.event, event.functionName));
      }
      return _results;
    };
    /**
     * @param {string} selector
     * @param {string} event
     * @param {string} functionName
     * @return {?}
     */
    Delegator.prototype._addEvent = function(selector, event, functionName) {
      var closure;
      var subReporter = this;
      /**
       * @return {?}
       */
      closure = function() {
        return subReporter[functionName].apply(subReporter, arguments);
      };
      if (selector === "" && Delegator._isCustomEvent(event)) {
        this.subscribe(event, closure);
      } else {
        this.element.delegate(selector, event, closure);
      }
      /** @type {function (): ?} */
      this._closures["" + selector + "/" + event + "/" + functionName] = closure;
      return this;
    };
    /**
     * @param {string} selector
     * @param {string} event
     * @param {string} functionName
     * @return {?}
     */
    Delegator.prototype._removeEvent = function(selector, event, functionName) {
      var closure;
      closure = this._closures["" + selector + "/" + event + "/" + functionName];
      if (selector === "" && Delegator._isCustomEvent(event)) {
        this.unsubscribe(event, closure);
      } else {
        this.element.undelegate(selector, event, closure);
      }
      delete this._closures["" + selector + "/" + event + "/" + functionName];
      return this;
    };
    /**
     * @return {?}
     */
    Delegator.prototype.publish = function() {
      this.element.triggerHandler.apply(this.element, arguments);
      return this;
    };
    /**
     * @param {string} event
     * @param {?} callback
     * @return {?}
     */
    Delegator.prototype.subscribe = function(event, callback) {
      var closure;
      /**
       * @return {?}
       */
      closure = function() {
        return callback.apply(this, [].slice.call(arguments, 1));
      };
      closure.guid = callback.guid = $.guid += 1;
      this.element.bind(event, closure);
      return this;
    };
    /**
     * @return {?}
     */
    Delegator.prototype.unsubscribe = function() {
      this.element.unbind.apply(this.element, arguments);
      return this;
    };
    return Delegator;
  }();
  /**
   * @param {Object} eventsObj
   * @return {?}
   */
  Delegator._parseEvents = function(eventsObj) {
    var event;
    var events;
    var functionName;
    var sel;
    var dig;
    var _i;
    var _ref;
    /** @type {Array} */
    events = [];
    for (sel in eventsObj) {
      functionName = eventsObj[sel];
      /** @type {Array.<string>} */
      _ref = sel.split(" ");
      /** @type {Array} */
      dig = 2 <= _ref.length ? __slice.call(_ref, 0, _i = _ref.length - 1) : (_i = 0, []);
      /** @type {string} */
      event = _ref[_i++];
      events.push({
        selector : dig.join(" "),
        event : event,
        functionName : functionName
      });
    }
    return events;
  };
  Delegator.natives = function() {
    var key;
    var caseSensitive;
    var val;
    caseSensitive = function() {
      var _ref;
      var tmp_args;
      _ref = jQuery.event.special;
      /** @type {Array} */
      tmp_args = [];
      for (key in _ref) {
        if (!__hasProp.call(_ref, key)) {
          continue;
        }
        val = _ref[key];
        tmp_args.push(key);
      }
      return tmp_args;
    }();
    return "blur focus focusin focusout load resize scroll unload click dblclick\nmousedown mouseup mousemove mouseover mouseout mouseenter mouseleave\nchange select submit keydown keypress keyup error".split(/[^a-z]+/).concat(caseSensitive);
  }();
  /**
   * @param {string} event
   * @return {?}
   */
  Delegator._isCustomEvent = function(event) {
    event = event.split(".")[0];
    return $.inArray(event, Delegator.natives) === -1;
  };
  Range = {};
  /**
   * @param {Object} r
   * @return {?}
   */
  Range.sniff = function(r) {
    if (r.commonAncestorContainer != null) {
      return new Range.BrowserRange(r);
    } else {
      if (typeof r.start === "string") {
        return new Range.SerializedRange(r);
      } else {
        if (r.start && typeof r.start === "object") {
          return new Range.NormalizedRange(r);
        } else {
          console.error(_t("Could not sniff range type"));
          return false;
        }
      }
    }
  };
  /**
   * @param {(number|string)} xpath
   * @param {Object} root
   * @return {?}
   */
  Range.nodeFromXPath = function(xpath, root) {
    var customResolver;
    var evaluateXPath;
    var namespace;
    var node;
    var cur;
    if (root == null) {
      /** @type {HTMLDocument} */
      root = document;
    }
    /**
     * @param {string} xp
     * @param {(Function|string)} nsResolver
     * @return {?}
     */
    evaluateXPath = function(xp, nsResolver) {
      var bulk;
      if (nsResolver == null) {
        /** @type {null} */
        nsResolver = null;
      }
      try {
        return document.evaluate("." + xp, root, nsResolver, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
      } catch (fn) {
        bulk = fn;
        console.log("XPath evaluation failed.");
        console.log("Trying fallback...");
        return Util.nodeFromXPath(xp, root);
      }
    };
    if (!$.isXMLDoc(document.documentElement)) {
      return evaluateXPath(xpath);
    } else {
      customResolver = document.createNSResolver(document.ownerDocument === null ? document.documentElement : document.ownerDocument.documentElement);
      node = evaluateXPath(xpath, customResolver);
      if (!node) {
        xpath = function() {
          var i;
          var valsLength;
          var codeSegments;
          var eventPath;
          codeSegments = xpath.split("/");
          /** @type {Array} */
          eventPath = [];
          /** @type {number} */
          i = 0;
          valsLength = codeSegments.length;
          for (;i < valsLength;i++) {
            cur = codeSegments[i];
            if (cur && cur.indexOf(":") === -1) {
              eventPath.push(cur.replace(/^([a-z]+)/, "xhtml:$1"));
            } else {
              eventPath.push(cur);
            }
          }
          return eventPath;
        }().join("/");
        /** @type {string} */
        namespace = document.lookupNamespaceURI(null);
        /**
         * @param {string} ns
         * @return {?}
         */
        customResolver = function(ns) {
          if (ns === "xhtml") {
            return namespace;
          } else {
            return document.documentElement.getAttribute("xmlns:" + ns);
          }
        };
        node = evaluateXPath(xpath, customResolver);
      }
      return node;
    }
  };
  Range.RangeError = function(_super) {
    /**
     * @param {string} type
     * @param {string} message
     * @param {string} parent
     * @return {undefined}
     */
    function RangeError(type, message, parent) {
      /** @type {string} */
      this.type = type;
      /** @type {string} */
      this.message = message;
      this.parent = parent != null ? parent : null;
      RangeError.__super__.constructor.call(this, this.message);
    }
    __extends(RangeError, _super);
    return RangeError;
  }(Error);
  Range.BrowserRange = function() {
    /**
     * @param {Object} obj
     * @return {undefined}
     */
    function BrowserRange(obj) {
      this.commonAncestorContainer = obj.commonAncestorContainer;
      this.startContainer = obj.startContainer;
      this.startOffset = obj.startOffset;
      this.endContainer = obj.endContainer;
      this.endOffset = obj.endOffset;
    }
    /**
     * @param {Object} logger
     * @return {?}
     */
    BrowserRange.prototype.normalize = function(logger) {
      var node;
      var n;
      var nr;
      var range;
      if (this.tainted) {
        console.error(_t("You may only call normalize() once on a BrowserRange!"));
        return false;
      } else {
        /** @type {boolean} */
        this.tainted = true;
      }
      range = {};
      if (this.startContainer.nodeType === Node.ELEMENT_NODE) {
        range.start = Util.getFirstTextNodeNotBefore(this.startContainer.childNodes[this.startOffset]);
        /** @type {number} */
        range.startOffset = 0;
      } else {
        range.start = this.startContainer;
        range.startOffset = this.startOffset;
      }
      if (this.endContainer.nodeType === Node.ELEMENT_NODE) {
        n = this.endContainer.childNodes[this.endOffset];
        if (n != null) {
          node = n;
          for (;node != null && node.nodeType !== Node.TEXT_NODE;) {
            node = node.firstChild;
          }
          if (node != null) {
            range.end = node;
            /** @type {number} */
            range.endOffset = 0;
          }
        }
        if (range.end == null) {
          n = this.endContainer.childNodes[this.endOffset - 1];
          range.end = Util.getLastTextNodeUpTo(n);
          range.endOffset = range.end.nodeValue.length;
        }
      } else {
        range.end = this.endContainer;
        range.endOffset = this.endOffset;
      }
      nr = {};
      if (range.startOffset > 0) {
        if (range.start.nodeValue.length > range.startOffset) {
          nr.start = range.start.splitText(range.startOffset);
        } else {
          nr.start = range.start.nextSibling;
        }
      } else {
        nr.start = range.start;
      }
      if (range.start === range.end) {
        if (nr.start.nodeValue.length > range.endOffset - range.startOffset) {
          nr.start.splitText(range.endOffset - range.startOffset);
        }
        nr.end = nr.start;
      } else {
        if (range.end.nodeValue.length > range.endOffset) {
          range.end.splitText(range.endOffset);
        }
        nr.end = range.end;
      }
      nr.commonAncestor = this.commonAncestorContainer;
      for (;nr.commonAncestor.nodeType !== Node.ELEMENT_NODE;) {
        nr.commonAncestor = nr.commonAncestor.parentNode;
      }
      return new Range.NormalizedRange(nr);
    };
    /**
     * @param {Object} root
     * @param {string} opt
     * @return {?}
     */
    BrowserRange.prototype.serialize = function(root, opt) {
      return this.normalize(root).serialize(root, opt);
    };
    return BrowserRange;
  }();
  Range.NormalizedRange = function() {
    /**
     * @param {?} obj
     * @return {undefined}
     */
    function NormalizedRange(obj) {
      this.commonAncestor = obj.commonAncestor;
      this.start = obj.start;
      this.end = obj.end;
    }
    /**
     * @param {Object} logger
     * @return {?}
     */
    NormalizedRange.prototype.normalize = function(logger) {
      return this;
    };
    /**
     * @param {?} bounds
     * @return {?}
     */
    NormalizedRange.prototype.limit = function(bounds) {
      var start;
      var x;
      var pos;
      var _i;
      var _len;
      var xs;
      start = $.grep(this.textNodes(), function(node) {
        return node.parentNode === bounds || $.contains(bounds, node.parentNode);
      });
      if (!start.length) {
        return null;
      }
      this.start = start[0];
      this.end = start[start.length - 1];
      pos = $(this.start).parents();
      xs = $(this.end).parents();
      /** @type {number} */
      _i = 0;
      _len = xs.length;
      for (;_i < _len;_i++) {
        x = xs[_i];
        if (pos.index(x) !== -1) {
          this.commonAncestor = x;
          break;
        }
      }
      return this;
    };
    /**
     * @param {Object} root
     * @param {string} opt
     * @return {?}
     */
    NormalizedRange.prototype.serialize = function(root, opt) {
      var end;
      var serialization;
      var start;
      /**
       * @param {Node} node
       * @param {boolean} dataAndEvents
       * @return {?}
       */
      serialization = function(node, dataAndEvents) {
        var key;
        var tmp_keys;
        var prevChunksLen;
        var origParent;
        var textNodes;
        var xpath;
        var i;
        var il;
        if (opt) {
          origParent = $(node).parents(":not(" + opt + ")").eq(0);
        } else {
          origParent = $(node).parent();
        }
        xpath = Util.xpathFromNode(origParent, root)[0];
        textNodes = Util.getTextNodes(origParent);
        tmp_keys = textNodes.slice(0, textNodes.index(node));
        /** @type {number} */
        prevChunksLen = 0;
        /** @type {number} */
        i = 0;
        il = tmp_keys.length;
        for (;i < il;i++) {
          key = tmp_keys[i];
          prevChunksLen += key.nodeValue.length;
        }
        if (dataAndEvents) {
          return[xpath, prevChunksLen + node.nodeValue.length];
        } else {
          return[xpath, prevChunksLen];
        }
      };
      start = serialization(this.start);
      end = serialization(this.end, true);
      return new Range.SerializedRange({
        start : start[0],
        end : end[0],
        startOffset : start[1],
        endOffset : end[1]
      });
    };
    /**
     * @return {?}
     */
    NormalizedRange.prototype.text = function() {
      var node;
      return function() {
        var _i;
        var _len;
        var scripts;
        var _results;
        scripts = this.textNodes();
        /** @type {Array} */
        _results = [];
        /** @type {number} */
        _i = 0;
        _len = scripts.length;
        for (;_i < _len;_i++) {
          node = scripts[_i];
          _results.push(node.nodeValue);
        }
        return _results;
      }.call(this).join("");
    };
    /**
     * @return {?}
     */
    NormalizedRange.prototype.textNodes = function() {
      var val;
      var start;
      var textNodes;
      var _ref;
      textNodes = Util.getTextNodes($(this.commonAncestor));
      /** @type {Array} */
      _ref = [textNodes.index(this.start), textNodes.index(this.end)];
      start = _ref[0];
      val = _ref[1];
      return $.makeArray(textNodes.slice(start, +val + 1 || 9E9));
    };
    /**
     * @return {?}
     */
    NormalizedRange.prototype.toRange = function() {
      var range;
      /** @type {(Range|null)} */
      range = document.createRange();
      range.setStartBefore(this.start);
      range.setEndAfter(this.end);
      return range;
    };
    return NormalizedRange;
  }();
  Range.SerializedRange = function() {
    /**
     * @param {Range} obj
     * @return {undefined}
     */
    function SerializedRange(obj) {
      this.start = obj.start;
      this.startOffset = obj.startOffset;
      this.end = obj.end;
      this.endOffset = obj.endOffset;
    }
    /**
     * @param {Object} root
     * @return {?}
     */
    SerializedRange.prototype.normalize = function(root) {
      var contains;
      var e;
      var offset;
      var node;
      var p;
      var range;
      var viewPortEnd;
      var tn;
      var _i;
      var _j;
      var _len;
      var _len1;
      var _ref;
      var _ref1;
      range = {};
      /** @type {Array} */
      _ref = ["start", "end"];
      /** @type {number} */
      _i = 0;
      /** @type {number} */
      _len = _ref.length;
      for (;_i < _len;_i++) {
        p = _ref[_i];
        try {
          node = Range.nodeFromXPath(this[p], root);
        } catch (d) {
          e = d;
          throw new Range.RangeError(p, "Error while finding " + p + " node: " + this[p] + ": " + e, e);
        }
        if (!node) {
          throw new Range.RangeError(p, "Couldn't find " + p + " node: " + this[p]);
        }
        /** @type {number} */
        offset = 0;
        viewPortEnd = this[p + "Offset"];
        if (p === "end") {
          viewPortEnd--;
        }
        _ref1 = Util.getTextNodes($(node));
        /** @type {number} */
        _j = 0;
        _len1 = _ref1.length;
        for (;_j < _len1;_j++) {
          tn = _ref1[_j];
          if (offset + tn.nodeValue.length > viewPortEnd) {
            range[p + "Container"] = tn;
            /** @type {number} */
            range[p + "Offset"] = this[p + "Offset"] - offset;
            break;
          } else {
            offset += tn.nodeValue.length;
          }
        }
        if (range[p + "Offset"] == null) {
          throw new Range.RangeError("" + p + "offset", "Couldn't find offset " + this[p + "Offset"] + " in element " + this[p]);
        }
      }
      /** @type {function (Object, ?): ?} */
      contains = document.compareDocumentPosition == null ? function(a, arr) {
        return a.contains(arr);
      } : function(container, element) {
        return container.compareDocumentPosition(element) & 16;
      };
      $(range.startContainer).parents().each(function() {
        if (contains(this, range.endContainer)) {
          range.commonAncestorContainer = this;
          return false;
        }
      });
      return(new Range.BrowserRange(range)).normalize(root);
    };
    /**
     * @param {Object} root
     * @param {string} opt
     * @return {?}
     */
    SerializedRange.prototype.serialize = function(root, opt) {
      return this.normalize(root).serialize(root, opt);
    };
    /**
     * @return {?}
     */
    SerializedRange.prototype.toObject = function() {
      return{
        start : this.start,
        startOffset : this.startOffset,
        end : this.end,
        endOffset : this.endOffset
      };
    };
    return SerializedRange;
  }();
  _Annotator = this.Annotator;
  Annotator = function(_super) {
    /**
     * @param {?} element
     * @param {?} dataAndEvents
     * @return {?}
     */
    function Annotator(element, dataAndEvents) {
      this.onDeleteAnnotation = __bind(this.onDeleteAnnotation, this);
      this.onEditAnnotation = __bind(this.onEditAnnotation, this);
      this.onAdderClick = __bind(this.onAdderClick, this);
      this.onAdderMousedown = __bind(this.onAdderMousedown, this);
      this.onHighlightMouseover = __bind(this.onHighlightMouseover, this);
      this.checkForEndSelection = __bind(this.checkForEndSelection, this);
      this.checkForStartSelection = __bind(this.checkForStartSelection, this);
      this.clearViewerHideTimer = __bind(this.clearViewerHideTimer, this);
      this.startViewerHideTimer = __bind(this.startViewerHideTimer, this);
      this.showViewer = __bind(this.showViewer, this);
      this.onEditorSubmit = __bind(this.onEditorSubmit, this);
      this.onEditorHide = __bind(this.onEditorHide, this);
      this.showEditor = __bind(this.showEditor, this);
      Annotator.__super__.constructor.apply(this, arguments);
      this.plugins = {};
      if (!Annotator.supported()) {
        return this;
      }
      if (!this.options.readOnly) {
        this._setupDocumentEvents();
      }
      this._setupWrapper()._setupViewer()._setupEditor();
      this._setupDynamicStyle();
      this.adder = $(this.html.adder).appendTo(this.wrapper).hide();
      Annotator._instances.push(this);
    }
    __extends(Annotator, _super);
    Annotator.prototype.events = {
//      ".annotator-adder button click" : "onAdderClick",
//      ".annotator-adder button mousedown" : "onAdderMousedown",
      ".annotator-adder click" : "onAdderClick",
      ".annotator-adder mousedown" : "onAdderMousedown",
      ".annotator-hl mouseover" : "onHighlightMouseover",
      ".annotator-hl mouseout" : "startViewerHideTimer"
    };
    Annotator.prototype.html = {
//      adder : '<div class="annotator-adder"><button>' + _t("Annotate") + "</button></div>",
      adder : '<div class="annotator-adder"><span class="glyphicon glyphicon-plus"></span><div class="annotator-arrow-clip"><span class="annotator-arrow"></span></div></div>',
//      wrapper : '<div class="annotator-wrapper"></div>'
    	  wrapper : ''
    };
    Annotator.prototype.options = {
      readOnly : false
    };
    Annotator.prototype.plugins = {};
    /** @type {null} */
    Annotator.prototype.editor = null;
    /** @type {null} */
    Annotator.prototype.viewer = null;
    /** @type {null} */
    Annotator.prototype.selectedRanges = null;
    /** @type {boolean} */
    Annotator.prototype.mouseIsDown = false;
    /** @type {boolean} */
    Annotator.prototype.ignoreMouseup = false;
    /** @type {null} */
    Annotator.prototype.viewerHideTimer = null;
    /**
     * @return {?}
     */
    Annotator.prototype._setupWrapper = function() {
      this.wrapper = $(this.html.wrapper);
      this.element.find("script").remove();
      this.element.wrapInner(this.wrapper);
//      this.wrapper = this.element.find(".annotator-wrapper");
      this.wrapper = $("body");
      return this;
    };
    /**
     * @return {?}
     */
    Annotator.prototype._setupViewer = function() {
      var _this = this;
      this.viewer = new Annotator.Viewer({
        readOnly : this.options.readOnly
      });
      this.viewer.hide().on("edit", this.onEditAnnotation).on("delete", this.onDeleteAnnotation).addField({
        /**
         * @param {Object} field
         * @param {Object} annotation
         * @return {?}
         */
        load : function(field, annotation) {
          if (annotation.text) {
            $(field).html(Util.escape(annotation.text));
          } else {
            $(field).html("<i>" + _t("No Comment") + "</i>");
          }
          return _this.publish("annotationViewerTextField", [field, annotation]);
        }
      }).element.appendTo(this.wrapper).bind({
        mouseover : this.clearViewerHideTimer,
        mouseout : this.startViewerHideTimer
      });
      return this;
    };
    /**
     * @return {?}
     */
    Annotator.prototype._setupEditor = function() {
      this.editor = new Annotator.Editor;
      this.editor.hide().on("hide", this.onEditorHide).on("save", this.onEditorSubmit).addField({
        type : "textarea",
        label : _t("Comments") + "\u2026",
        /**
         * @param {Object} field
         * @param {Object} annotation
         * @return {?}
         */
        load : function(field, annotation) {
          return $(field).find("textarea").val(annotation.text || "");
        },
        /**
         * @param {?} element
         * @param {Object} elem
         * @return {?}
         */
        submit : function(element, elem) {
          return elem.text = $(element).find("textarea").val();
        }
      });
      this.editor.element.appendTo(this.wrapper);
      return this;
    };
    /**
     * @return {?}
     */
    Annotator.prototype._setupDocumentEvents = function() {
      $(document).bind({
        mouseup : this.checkForEndSelection,
        mousedown : this.checkForStartSelection
      });
      return this;
    };
    /**
     * @return {?}
     */
    Annotator.prototype._setupDynamicStyle = function() {
      var closingAnimationTime;
      var sel;
      var style;
      var elementListeners;
      style = $("#annotator-dynamic-style");
      if (!style.length) {
        style = $('<style id="annotator-dynamic-style"></style>').appendTo(document.head);
      }
      sel = "*" + function() {
        var _i;
        var _len;
        var _ref;
        var _results;
        /** @type {Array} */
        _ref = ["adder", "outer", "notice", "filter"];
        /** @type {Array} */
        _results = [];
        /** @type {number} */
        _i = 0;
        /** @type {number} */
        _len = _ref.length;
        for (;_i < _len;_i++) {
          elementListeners = _ref[_i];
          _results.push(":not(.annotator-" + elementListeners + ")");
        }
        return _results;
      }().join("");
      closingAnimationTime = Util.maxZIndex($(document.body).find(sel));
      /** @type {number} */
      closingAnimationTime = Math.max(closingAnimationTime, 1E3);
      style.text([".annotator-adder, .annotator-outer, .annotator-notice {", "  z-index: " + (closingAnimationTime + 20) + ";", "}", ".annotator-filter {", "  z-index: " + (closingAnimationTime + 10) + ";", "}"].join("\n"));
      return this;
    };
    /**
     * @return {?}
     */
    Annotator.prototype.destroy = function() {
      var fromIndex;
      var key;
      var val;
      var _base;
      var _ref;
      Annotator.__super__.destroy.apply(this, arguments);
      $(document).unbind({
        mouseup : this.checkForEndSelection,
        mousedown : this.checkForStartSelection
      });
      $("#annotator-dynamic-style").remove();
      this.adder.remove();
      this.viewer.destroy();
      this.editor.destroy();
      this.wrapper.find(".annotator-hl").each(function() {
        $(this).contents().insertBefore(this);
        return $(this).remove();
      });
      this.wrapper.contents().insertBefore(this.wrapper);
      this.wrapper.remove();
      this.element.data("annotator", null);
      _ref = this.plugins;
      for (key in _ref) {
        val = _ref[key];
        if (typeof(_base = this.plugins[key]).destroy === "function") {
          _base.destroy();
        }
      }
      fromIndex = Annotator._instances.indexOf(this);
      if (fromIndex !== -1) {
        return Annotator._instances.splice(fromIndex, 1);
      }
    };
    /**
     * @return {?}
     */
    Annotator.prototype.getSelectedRanges = function() {
      var browserRange;
      var i;
      var vvar;
      var r;
      var elements;
      var rangesToIgnore;
      var selection;
      var _i;
      var _len;
      selection = Util.getGlobal().getSelection();
      /** @type {Array} */
      elements = [];
      /** @type {Array} */
      rangesToIgnore = [];
      if (!selection.isCollapsed) {
        elements = function() {
          var _i;
          var _ref;
          var assigns;
          /** @type {Array} */
          assigns = [];
          /** @type {number} */
          i = _i = 0;
          _ref = selection.rangeCount;
          for (;0 <= _ref ? _i < _ref : _i > _ref;i = 0 <= _ref ? ++_i : --_i) {
            r = selection.getRangeAt(i);
            browserRange = new Range.BrowserRange(r);
            vvar = browserRange.normalize().limit(this.wrapper[0]);
            if (vvar === null) {
              rangesToIgnore.push(r);
            }
            assigns.push(vvar);
          }
          return assigns;
        }.call(this);
        selection.removeAllRanges();
      }
      /** @type {number} */
      _i = 0;
      /** @type {number} */
      _len = rangesToIgnore.length;
      for (;_i < _len;_i++) {
        r = rangesToIgnore[_i];
        selection.addRange(r);
      }
      return $.grep(elements, function(range) {
        if (range) {
          selection.addRange(range.toRange());
        }
        return range;
      });
    };

    /**
     * @return {?}
     */
    Annotator.prototype.createAnnotation = function() {
      var annotation;
      annotation = {};
      this.publish("beforeAnnotationCreated", [annotation]);
      return annotation;
    };
    /**
     * @param {Object} annotation
     * @return {?}
     */
    Annotator.prototype.setupAnnotation = function(annotation) {
        console.warn('setupAnnotation', annotation);
      var e;
      var normed;
      var normedRanges;
      var r;
      var root;
      var _i;
      var _l;
      var _len;
      var _len3;
      var _ref;
      root = this.wrapper[0];
      if (!annotation.ranges) {
        annotation.ranges = this.selectedRanges;
      }
      /** @type {Array} */
      normedRanges = [];
      _ref = annotation.ranges;
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        r = _ref[_i];
        try {
          normedRanges.push(Range.sniff(r).normalize(root));
        } catch (d) {
          e = d;
          if (e instanceof Range.RangeError) {
            this.publish("rangeNormalizeFail", [annotation, r, e]);
          } else {
            throw e;
          }
        }
      }
      /** @type {Array} */
      annotation.quote = [];
      /** @type {Array} */
      annotation.ranges = [];
      /** @type {Array} */
      annotation.highlights = [];
      /** @type {number} */
      _l = 0;
      /** @type {number} */
      _len3 = normedRanges.length;
      for (;_l < _len3;_l++) {
        normed = normedRanges[_l];
        annotation.quote.push($.trim(normed.text()));
        annotation.ranges.push(normed.serialize(this.wrapper[0], ".annotator-hl"));
        $.merge(annotation.highlights, this.highlightRange(normed));
      }
      /** @type {string} */
      annotation.quote = annotation.quote.join(" / ");
      $(annotation.highlights).data("annotation", annotation);
      $(annotation.highlights).attr("data-annotation-id", annotation.id);
      return annotation;
    };
    /**
     * @param {Object} annotation
     * @return {?}
     */
    Annotator.prototype.updateAnnotation = function(annotation) {
      this.publish("beforeAnnotationUpdated", [annotation]);
      $(annotation.highlights).attr("data-annotation-id", annotation.id);
      this.publish("annotationUpdated", [annotation]);
      return annotation;
    };
    /**
     * @param {?} annotation
     * @return {?}
     */
    Annotator.prototype.deleteAnnotation = function(annotation) {
      var child;
      var h;
      var i;
      var valsLength;
      var highlights;
      if (annotation.highlights != null) {
        highlights = annotation.highlights;
        /** @type {number} */
        i = 0;
        valsLength = highlights.length;
        for (;i < valsLength;i++) {
          h = highlights[i];
          if (!(h.parentNode != null)) {
            continue;
          }
          child = h.childNodes[0];
          $(h).replaceWith(h.childNodes);
        }
      }
      this.publish("annotationDeleted", [annotation]);
      return annotation;
    };
    /**
     * @param {Object} annotations
     * @return {?}
     */
    Annotator.prototype.loadAnnotations = function(annotations) {
      var clone;
      var loader;
      var _this = this;
      if (annotations == null) {
        /** @type {Array} */
        annotations = [];
      }
      /**
       * @param {Array} annList
       * @return {?}
       */
      loader = function(annList) {
        var n;
        var codeSegments;
        var i;
        var valsLength;
        if (annList == null) {
          /** @type {Array} */
          annList = [];
        }
        codeSegments = annList.splice(0, 10);
        /** @type {number} */
        i = 0;
        valsLength = codeSegments.length;
        for (;i < valsLength;i++) {
          n = codeSegments[i];
          _this.setupAnnotation(n);
        }
        if (annList.length > 0) {
          return setTimeout(function() {
            return loader(annList);
          }, 10);
        } else {
          return _this.publish("annotationsLoaded", [clone]);
        }
      };
      clone = annotations.slice();
      loader(annotations);
      return this;
    };
    /**
     * @return {?}
     */
    Annotator.prototype.dumpAnnotations = function() {
      if (this.plugins["Store"]) {
        return this.plugins["Store"].dumpAnnotations();
      } else {
        console.warn(_t("Can't dump annotations without Store plugin."));
        return false;
      }
    };
    /**
     * @param {?} normedRange
     * @param {string} cssClass
     * @return {?}
     */
    Annotator.prototype.highlightRange = function(normedRange, cssClass) {
      var html;
      var node;
      var nocode;
      var _i;
      var _len;
      var scripts;
      var _results;
      if (cssClass == null) {
        /** @type {string} */
        cssClass = "annotator-hl";
      }
      /** @type {RegExp} */
      nocode = /^\s*$/;
      html = $("<span class='" + cssClass + "'></span>");
      scripts = normedRange.textNodes();
      /** @type {Array} */
      _results = [];
      /** @type {number} */
      _i = 0;
      _len = scripts.length;
      for (;_i < _len;_i++) {
        node = scripts[_i];
        if (!nocode.test(node.nodeValue)) {
          _results.push($(node).wrapAll(html).parent().show()[0]);
        }
      }
      return _results;
    };
    /**
     * @param {Array} normedRanges
     * @param {string} cssClass
     * @return {?}
     */
    Annotator.prototype.highlightRanges = function(normedRanges, cssClass) {
      var nodes;
      var r;
      var _i;
      var _len;
      if (cssClass == null) {
        /** @type {string} */
        cssClass = "annotator-hl";
      }
      /** @type {Array} */
      nodes = [];
      /** @type {number} */
      _i = 0;
      _len = normedRanges.length;
      for (;_i < _len;_i++) {
        r = normedRanges[_i];
        $.merge(nodes, this.highlightRange(r, cssClass));
      }
      return nodes;
    };
    /**
     * @param {?} name
     * @param {string} options
     * @return {?}
     */
    Annotator.prototype.addPlugin = function(name, options) {
      var klass;
      var _base;
      if (this.plugins[name]) {
        console.error(_t("You cannot have more than one instance of any plugin."));
      } else {
        klass = Annotator.Plugin[name];
        if (typeof klass === "function") {
          this.plugins[name] = new klass(this.element[0], options);
          this.plugins[name].annotator = this;
          if (typeof(_base = this.plugins[name]).pluginInit === "function") {
            _base.pluginInit();
          }
        } else {
          console.error(_t("Could not load ") + name + _t(" plugin. Have you included the appropriate <script> tag?"));
        }
      }
      return this;
    };
    /**
     * @param {Object} annotation
     * @param {?} value
     * @return {?}
     */
    Annotator.prototype.showEditor = function(annotation, value) {
      this.editor.element.css(value);
      this.editor.load(annotation);
      this.publish("annotationEditorShown", [this.editor, annotation]);
      return this;
    };
    /**
     * @return {?}
     */
    Annotator.prototype.onEditorHide = function() {
      this.publish("annotationEditorHidden", [this.editor]);
      return this.ignoreMouseup = false;
    };
    /**
     * @param {?} annotation
     * @return {?}
     */
    Annotator.prototype.onEditorSubmit = function(annotation) {
      return this.publish("annotationEditorSubmit", [this.editor, annotation]);
    };
    /**
     * @param {Object} annotations
     * @param {?} location
     * @return {?}
     */
    Annotator.prototype.showViewer = function(annotations, location) {
      this.viewer.element.css(location);
      this.viewer.load(annotations);
      return this.publish("annotationViewerShown", [this.viewer, annotations]);
    };
    /**
     * @return {?}
     */
    Annotator.prototype.startViewerHideTimer = function() {
      if (!this.viewerHideTimer) {
        return this.viewerHideTimer = setTimeout(this.viewer.hide, 250);
      }
    };
    /**
     * @return {?}
     */
    Annotator.prototype.clearViewerHideTimer = function() {
      clearTimeout(this.viewerHideTimer);
      return this.viewerHideTimer = false;
    };
    /**
     * @param {Object} event
     * @return {?}
     */
    Annotator.prototype.checkForStartSelection = function(event) {
        console.warn('checkForStartSelection');
      if (!(event && this.isAnnotator(event.target))) {
        this.startViewerHideTimer();
      }
      return this.mouseIsDown = true;
    };
    /**
     * @param {number} event
     * @return {?}
     */
    Annotator.prototype.checkForEndSelection = function(event) {
        console.warn('checkForEndSelection');
      var container;
      var range;
      var _i;
      var _len;
      var _ref;
      /** @type {boolean} */
      this.mouseIsDown = false;
      if (this.ignoreMouseup) {
        return;
      }
      this.selectedRanges = this.getSelectedRanges();
      _ref = this.selectedRanges;
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        range = _ref[_i];
        container = range.commonAncestor;
        if (this.isAnnotator(container)) {
          return;
        }
      }
      if (event && this.selectedRanges.length) {
        console.log("selected");
        // 아래 주석 해제 하면, 드래그 동작만으로 커멘트 추가 가능 ... 현재는 일부러 꺼둔 상태입니다. (우클릭하여 컨텍스트 메뉴로만 가능)
        //return this.adder.css(Util.mousePosition(event, this.wrapper[0])).show();
      } else {
        return this.adder.hide();
      }
    };
    /**
     * @param {?} element
     * @return {?}
     */
    Annotator.prototype.isAnnotator = function(element) {
      return!!$(element).parents().addBack().filter("[class^=annotator-]").not("[class=annotator-hl]").not(this.wrapper).length;
    };
    /**
     * @param {Object} event
     * @return {?}
     */
    Annotator.prototype.onHighlightMouseover = function(event) {
      var annotations;
      this.clearViewerHideTimer();
      if (this.mouseIsDown) {
        return false;
      }
      if (this.viewer.isShown()) {
        this.viewer.hide();
      }
      annotations = $(event.target).parents(".annotator-hl").addBack().map(function() {
        return $(this).data("annotation");
      }).toArray();
      return this.showViewer(annotations, Util.mousePosition(event, this.wrapper[0]));
    };
    /**
     * @param {Function} event
     * @return {?}
     */
    Annotator.prototype.onAdderMousedown = function(event) {
      if (event != null) {
        event.preventDefault();
      }
      return this.ignoreMouseup = true;
    };
    /**
     * @param {Function} event
     * @return {?}
     */
    Annotator.prototype.onAdderClick = function(event) {
      var annotation;
      var cancel;
      var cleanup;
      var udataCur;
      var save;
      var _this = this;
      if (event != null) {
        event.preventDefault();
      }
      udataCur = this.adder.position();
      this.adder.hide();

      var createdAnnotation = this.createAnnotation();
      console.warn('createdAnnotation', createdAnnotation);
      if (createdAnnotation.ranges == null ) {
          this.checkForEndSelection();
          createdAnnotation = this.createAnnotation();
      }
      annotation = this.setupAnnotation(createdAnnotation);
      $(annotation.highlights).addClass("annotator-hl-temporary");
      /**
       * @return {?}
       */
      save = function() {
        cleanup();
        $(annotation.highlights).removeClass("annotator-hl-temporary");
        return _this.publish("annotationCreated", [annotation]);
      };
      /**
       * @return {?}
       */
      cancel = function() {
        cleanup();
        return _this.deleteAnnotation(annotation);
      };
      /**
       * @return {?}
       */
      cleanup = function() {
        _this.unsubscribe("annotationEditorHidden", cancel);
        return _this.unsubscribe("annotationEditorSubmit", save);
      };
      this.subscribe("annotationEditorHidden", cancel);
      this.subscribe("annotationEditorSubmit", save);
      return this.showEditor(annotation, udataCur);
    };
    /**
     * @param {Object} annotation
     * @return {?}
     */
    Annotator.prototype.onEditAnnotation = function(annotation) {
      var cleanup;
      var udataCur;
      var update;
      var _this = this;
      udataCur = this.viewer.element.position();
      /**
       * @return {?}
       */
      update = function() {
        cleanup();
        return _this.updateAnnotation(annotation);
      };
      /**
       * @return {?}
       */
      cleanup = function() {
        _this.unsubscribe("annotationEditorHidden", cleanup);
        return _this.unsubscribe("annotationEditorSubmit", update);
      };
      this.subscribe("annotationEditorHidden", cleanup);
      this.subscribe("annotationEditorSubmit", update);
      this.viewer.hide();
      return this.showEditor(annotation, udataCur);
    };
    /**
     * @param {?} annotation
     * @return {?}
     */
    Annotator.prototype.onDeleteAnnotation = function(annotation) {
      this.viewer.hide();
      return this.deleteAnnotation(annotation);
    };
    return Annotator;
  }(Delegator);
  Annotator.Plugin = function(_super) {
    /**
     * @param {?} dataAndEvents
     * @param {?} deepDataAndEvents
     * @return {undefined}
     */
    function Unsupported(dataAndEvents, deepDataAndEvents) {
      Unsupported.__super__.constructor.apply(this, arguments);
    }
    __extends(Unsupported, _super);
    /**
     * @return {undefined}
     */
    Unsupported.prototype.pluginInit = function() {
    };
    return Unsupported;
  }(Delegator);
  g = Util.getGlobal();
  if (((_ref1 = g.document) != null ? _ref1.evaluate : void 0) == null) {
    $.getScript("http://assets.annotateit.org/vendor/xpath.min.js");
  }
  if (g.getSelection == null) {
    $.getScript("http://assets.annotateit.org/vendor/ierange.min.js");
  }
  if (g.JSON == null) {
    $.getScript("http://assets.annotateit.org/vendor/json2.min.js");
  }
  if (g.Node == null) {
    g.Node = {
      ELEMENT_NODE : 1,
      ATTRIBUTE_NODE : 2,
      TEXT_NODE : 3,
      CDATA_SECTION_NODE : 4,
      ENTITY_REFERENCE_NODE : 5,
      ENTITY_NODE : 6,
      PROCESSING_INSTRUCTION_NODE : 7,
      COMMENT_NODE : 8,
      DOCUMENT_NODE : 9,
      DOCUMENT_TYPE_NODE : 10,
      DOCUMENT_FRAGMENT_NODE : 11,
      NOTATION_NODE : 12
    };
  }
  Annotator.$ = $;
  Annotator.Delegator = Delegator;
  Annotator.Range = Range;
  Annotator.Util = Util;
  /** @type {Array} */
  Annotator._instances = [];
  /** @type {function (string): ?} */
  Annotator._t = _t;
  /**
   * @return {?}
   */
  Annotator.supported = function() {
    return function() {
      return!!this.getSelection;
    }();
  };
  /**
   * @return {?}
   */
  Annotator.noConflict = function() {
    Util.getGlobal().Annotator = _Annotator;
    return this;
  };
  /**
   * @param {string} options
   * @return {?}
   */
  $.fn.annotator = function(options) {
    var ret;
    /** @type {Array.<?>} */
    ret = Array.prototype.slice.call(arguments, 1);
    return this.each(function() {
      var instance;
      instance = $.data(this, "annotator");
      if (options === "destroy") {
        $.removeData(this, "annotator");
        return instance != null ? instance.destroy(ret) : void 0;
      } else {
        if (instance) {
          return options && instance[options].apply(instance, ret);
        } else {
          instance = new Annotator(this, options);
          return $.data(this, "annotator", instance);
        }
      }
    });
  };
  this.Annotator = Annotator;
  Annotator.Widget = function(_super) {
    /**
     * @param {?} config
     * @param {?} element
     * @return {undefined}
     */
    function Widget(config, element) {
      Widget.__super__.constructor.apply(this, arguments);
      this.classes = $.extend({}, Annotator.Widget.prototype.classes, this.classes);
    }
    __extends(Widget, _super);
    Widget.prototype.classes = {
      hide : "annotator-hide",
      invert : {
        x : "annotator-invert-x",
        y : "annotator-invert-y"
      }
    };
    /**
     * @return {?}
     */
    Widget.prototype.destroy = function() {
      this.removeEvents();
      return this.element.remove();
    };
    /**
     * @return {?}
     */
    Widget.prototype.checkOrientation = function() {
      var current;
      var iniPos;
      var viewport;
      var q;
      var $win;
      this.resetOrientation();
      $win = $(Annotator.Util.getGlobal());
      q = this.element.children(":first");
      iniPos = q.offset();
      viewport = {
        top : $win.scrollTop(),
        right : $win.width() + $win.scrollLeft()
      };
      current = {
        top : iniPos.top,
        right : iniPos.left + q.width()
      };
      if (current.top - viewport.top < 0) {
        this.invertY();
      }
      if (current.right - viewport.right > 0) {
        this.invertX();
      }
      return this;
    };
    /**
     * @return {?}
     */
    Widget.prototype.resetOrientation = function() {
      this.element.removeClass(this.classes.invert.x).removeClass(this.classes.invert.y);
      return this;
    };
    /**
     * @return {?}
     */
    Widget.prototype.invertX = function() {
      this.element.addClass(this.classes.invert.x);
      return this;
    };
    /**
     * @return {?}
     */
    Widget.prototype.invertY = function() {
      this.element.addClass(this.classes.invert.y);
      return this;
    };
    /**
     * @return {?}
     */
    Widget.prototype.isInvertedY = function() {
      return this.element.hasClass(this.classes.invert.y);
    };
    /**
     * @return {?}
     */
    Widget.prototype.isInvertedX = function() {
      return this.element.hasClass(this.classes.invert.x);
    };
    return Widget;
  }(Delegator);
  Annotator.Editor = function(_super) {
    /**
     * @param {?} config
     * @return {undefined}
     */
    function Editor(config) {
      this.onCancelButtonMouseover = __bind(this.onCancelButtonMouseover, this);
      this.processKeypress = __bind(this.processKeypress, this);
      this.submit = __bind(this.submit, this);
      this.load = __bind(this.load, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      Editor.__super__.constructor.call(this, $(this.html)[0], config);
      /** @type {Array} */
      this.fields = [];
      this.annotation = {};
    }
    __extends(Editor, _super);
    Editor.prototype.events = {
      "form submit" : "submit",
      ".annotator-save click" : "submit",
      ".annotator-cancel click" : "hide",
      ".annotator-cancel mouseover" : "onCancelButtonMouseover",
      "textarea keydown" : "processKeypress"
    };
    Editor.prototype.classes = {
      hide : "annotator-hide",
      focus : "annotator-focus"
    };
    /** @type {string} */
    Editor.prototype.html = '<div class="annotator-outer annotator-editor"><form class="annotator-widget"><ul class="annotator-listing"></ul><div class="annotator-controls"><a href="#cancel" class="annotator-cancel">' + _t("Cancel") + '</a><a href="#save" class="annotator-save annotator-focus">' + _t("Save") + "</a></div></form></div>";
    Editor.prototype.options = {};
    /**
     * @param {?} event
     * @return {?}
     */
    Editor.prototype.show = function(event) {
      Annotator.Util.preventEventDefault(event);
      this.element.removeClass(this.classes.hide);
      this.element.find(".annotator-save").addClass(this.classes.focus);
      this.checkOrientation();
      this.element.find(":input:first").focus();
      this.setupDraggables();
      return this.publish("show");
    };
    /**
     * @param {Function} event
     * @return {?}
     */
    Editor.prototype.hide = function(event) {
      Annotator.Util.preventEventDefault(event);
      this.element.addClass(this.classes.hide);
      return this.publish("hide");
    };
    /**
     * @param {?} annotation
     * @return {?}
     */
    Editor.prototype.load = function(annotation) {
      var field;
      var _i;
      var _len;
      var _ref;
      this.annotation = annotation;
      this.publish("load", [this.annotation]);
      _ref = this.fields;
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        field = _ref[_i];
        field.load(field.element, this.annotation);
      }
      return this.show();
    };
    /**
     * @param {Function} event
     * @return {?}
     */
    Editor.prototype.submit = function(event) {
      var field;
      var _i;
      var _len;
      var _ref;
      Annotator.Util.preventEventDefault(event);
      _ref = this.fields;
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        field = _ref[_i];
        field.submit(field.element, this.annotation);
      }
      this.publish("save", [this.annotation]);
      return this.hide();
    };
    /**
     * @param {?} opt_attributes
     * @return {?}
     */
    Editor.prototype.addField = function(opt_attributes) {
      var element;
      var field;
      var input;
      field = $.extend({
        id : "annotator-field-" + Annotator.Util.uuid(),
        type : "input",
        label : "",
        /**
         * @return {undefined}
         */
        load : function() {
        },
        /**
         * @return {undefined}
         */
        submit : function() {
        }
      }, opt_attributes);
      /** @type {null} */
      input = null;
      element = $('<li class="annotator-item" />');
      field.element = element[0];
      switch(field.type) {
        case "textarea":
          input = $("<textarea />");
          break;
        case "input":
        case "checkbox":
          input = $("<input />");
          break;
        case "select":
          input = $("<select />");
      }
      element.append(input);
      input.attr({
        id : field.id,
        placeholder : field.label
      });
      if (field.type === "checkbox") {
        /** @type {string} */
        input[0].type = "checkbox";
        element.addClass("annotator-checkbox");
        element.append($("<label />", {
          "for" : field.id,
          html : field.label
        }));
      }
      this.element.find("ul:first").append(element);
      this.fields.push(field);
      return field.element;
    };
    /**
     * @return {?}
     */
    Editor.prototype.checkOrientation = function() {
      var label;
      var input;
      Editor.__super__.checkOrientation.apply(this, arguments);
      input = this.element.find("ul");
      label = this.element.find(".annotator-controls");
      if (this.element.hasClass(this.classes.invert.y)) {
        label.insertBefore(input);
      } else {
        if (label.is(":first-child")) {
          label.insertAfter(input);
        }
      }
      return this;
    };
    /**
     * @param {Event} event
     * @return {?}
     */
    Editor.prototype.processKeypress = function(event) {
      if (event.keyCode === 27) {
        return this.hide();
      } else {
        if (event.keyCode === 13 && !event.shiftKey) {
          return this.submit();
        }
      }
    };
    /**
     * @return {?}
     */
    Editor.prototype.onCancelButtonMouseover = function() {
      return this.element.find("." + this.classes.focus).removeClass(this.classes.focus);
    };
    /**
     * @return {?}
     */
    Editor.prototype.setupDraggables = function() {
      var classes;
      var controls;
      var $sandbox;
      var editor;
      var mousedown;
      var onMousedown;
      var onMousemove;
      var onMouseup;
      var resize;
      var $content;
      var throttle;
      var _this = this;
      this.element.find(".annotator-resize").remove();
      if (this.element.hasClass(this.classes.invert.y)) {
        $sandbox = this.element.find(".annotator-item:last");
      } else {
        $sandbox = this.element.find(".annotator-item:first");
      }
      if ($sandbox) {
        $('<span class="annotator-resize"></span>').appendTo($sandbox);
      }
      /** @type {null} */
      mousedown = null;
      classes = this.classes;
      editor = this.element;
      /** @type {null} */
      $content = null;
      resize = editor.find(".annotator-resize");
      controls = editor.find(".annotator-controls");
      /** @type {boolean} */
      throttle = false;
      /**
       * @param {Object} event
       * @return {?}
       */
      onMousedown = function(event) {
        if (event.target === this) {
          mousedown = {
            element : this,
            top : event.pageY,
            left : event.pageX
          };
          $content = editor.find("textarea:first");
          $(window).bind({
            "mouseup.annotator-editor-resize" : onMouseup,
            "mousemove.annotator-editor-resize" : onMousemove
          });
          return event.preventDefault();
        }
      };
      /**
       * @return {?}
       */
      onMouseup = function() {
        /** @type {null} */
        mousedown = null;
        return $(window).unbind(".annotator-editor-resize");
      };
      /**
       * @param {Touch} event
       * @return {?}
       */
      onMousemove = function(event) {
        var diff;
        var directionX;
        var directionY;
        var contentHeight;
        var width;
        if (mousedown && throttle === false) {
          diff = {
            top : event.pageY - mousedown.top,
            left : event.pageX - mousedown.left
          };
          if (mousedown.element === resize[0]) {
            contentHeight = $content.outerHeight();
            width = $content.outerWidth();
            /** @type {number} */
            directionX = editor.hasClass(classes.invert.x) ? -1 : 1;
            /** @type {number} */
            directionY = editor.hasClass(classes.invert.y) ? 1 : -1;
            $content.height(contentHeight + diff.top * directionY);
            $content.width(width + diff.left * directionX);
            if ($content.outerHeight() !== contentHeight) {
              mousedown.top = event.pageY;
            }
            if ($content.outerWidth() !== width) {
              mousedown.left = event.pageX;
            }
          } else {
            if (mousedown.element === controls[0]) {
              editor.css({
                top : parseInt(editor.css("top"), 10) + diff.top,
                left : parseInt(editor.css("left"), 10) + diff.left
              });
              mousedown.top = event.pageY;
              mousedown.left = event.pageX;
            }
          }
          /** @type {boolean} */
          throttle = true;
          return setTimeout(function() {
            return throttle = false;
          }, 1E3 / 60);
        }
      };
      resize.bind("mousedown", onMousedown);
      return controls.bind("mousedown", onMousedown);
    };
    return Editor;
  }(Annotator.Widget);
  Annotator.Viewer = function(_super) {
    /**
     * @param {?} options
     * @return {undefined}
     */
    function Viewer(options) {
      this.onDeleteClick = __bind(this.onDeleteClick, this);
      this.onEditClick = __bind(this.onEditClick, this);
      this.load = __bind(this.load, this);
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      Viewer.__super__.constructor.call(this, $(this.html.element)[0], options);
      this.item = $(this.html.item)[0];
      /** @type {Array} */
      this.fields = [];
      /** @type {Array} */
      this.annotations = [];
    }
    __extends(Viewer, _super);
    Viewer.prototype.events = {
      ".annotator-edit click" : "onEditClick",
      ".annotator-delete click" : "onDeleteClick"
    };
    Viewer.prototype.classes = {
      hide : "annotator-hide",
      showControls : "annotator-visible"
    };
    Viewer.prototype.html = {
      element : '<div class="annotator-outer annotator-viewer">\n  <ul class="annotator-widget annotator-listing"></ul>\n</div>',
      item : '<li class="annotator-annotation annotator-item">\n  <span class="annotator-controls">\n    <a href="#" title="View as webpage" class="annotator-link">View as webpage</a>\n    <button title="Edit" class="annotator-edit">Edit</button>\n    <button title="Delete" class="annotator-delete">Delete</button>\n  </span>\n</li>'
    };
    Viewer.prototype.options = {
      readOnly : false
    };
    /**
     * @param {?} event
     * @return {?}
     */
    Viewer.prototype.show = function(event) {
      var controls;
      var _this = this;
      Annotator.Util.preventEventDefault(event);
      controls = this.element.find(".annotator-controls").addClass(this.classes.showControls);
      setTimeout(function() {
        return controls.removeClass(_this.classes.showControls);
      }, 500);
      this.element.removeClass(this.classes.hide);
      return this.checkOrientation().publish("show");
    };
    /**
     * @return {?}
     */
    Viewer.prototype.isShown = function() {
      return!this.element.hasClass(this.classes.hide);
    };
    /**
     * @param {Function} event
     * @return {?}
     */
    Viewer.prototype.hide = function(event) {
      Annotator.Util.preventEventDefault(event);
      this.element.addClass(this.classes.hide);
      return this.publish("hide");
    };
    /**
     * @param {Array} annotations
     * @return {?}
     */
    Viewer.prototype.load = function(annotations) {
      var annotation;
      var controller;
      var rule;
      var $name;
      var edit;
      var element;
      var field;
      var item;
      var link;
      var links;
      var form;
      var _i;
      var _l;
      var _len;
      var _len3;
      var _ref;
      var fields;
      this.annotations = annotations || [];
      form = this.element.find("ul:first").empty();
      _ref = this.annotations;
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        annotation = _ref[_i];
        item = $(this.item).clone().appendTo(form).data("annotation", annotation);
        rule = item.find(".annotator-controls");
        link = rule.find(".annotator-link");
        edit = rule.find(".annotator-edit");
        $name = rule.find(".annotator-delete");
        links = (new LinkParser(annotation.links || [])).get("alternate", {
          type : "text/html"
        });
        if (links.length === 0 || links[0].href == null) {
          link.remove();
        } else {
          link.attr("href", links[0].href);
        }
        if (this.options.readOnly) {
          edit.remove();
          $name.remove();
        } else {
          controller = {
            /**
             * @return {?}
             */
            showEdit : function() {
              return edit.removeAttr("disabled");
            },
            /**
             * @return {?}
             */
            hideEdit : function() {
              return edit.attr("disabled", "disabled");
            },
            /**
             * @return {?}
             */
            showDelete : function() {
              return $name.removeAttr("disabled");
            },
            /**
             * @return {?}
             */
            hideDelete : function() {
              return $name.attr("disabled", "disabled");
            }
          };
        }
        fields = this.fields;
        /** @type {number} */
        _l = 0;
        _len3 = fields.length;
        for (;_l < _len3;_l++) {
          field = fields[_l];
          element = $(field.element).clone().appendTo(item)[0];
          field.load(element, annotation, controller);
        }
      }
      this.publish("load", [this.annotations]);
      return this.show();
    };
    /**
     * @param {?} opt_attributes
     * @return {?}
     */
    Viewer.prototype.addField = function(opt_attributes) {
      var i;
      i = $.extend({
        /**
         * @return {undefined}
         */
        load : function() {
        }
      }, opt_attributes);
      i.element = $("<div />")[0];
      this.fields.push(i);
      i.element;
      return this;
    };
    /**
     * @param {Event} event
     * @return {?}
     */
    Viewer.prototype.onEditClick = function(event) {
      return this.onButtonClick(event, "edit");
    };
    /**
     * @param {Event} event
     * @return {?}
     */
    Viewer.prototype.onDeleteClick = function(event) {
      return this.onButtonClick(event, "delete");
    };
    /**
     * @param {Event} event
     * @param {string} type
     * @return {?}
     */
    Viewer.prototype.onButtonClick = function(event, type) {
      var _this;
      _this = $(event.target).parents(".annotator-annotation");
      return this.publish(type, [_this.data("annotation")]);
    };
    return Viewer;
  }(Annotator.Widget);
  LinkParser = function() {
    /**
     * @param {Object} data
     * @return {undefined}
     */
    function LinkParser(data) {
      /** @type {Object} */
      this.data = data;
    }
    /**
     * @param {string} rel
     * @param {Object} cond
     * @return {?}
     */
    LinkParser.prototype.get = function(rel, cond) {
      var d;
      var k;
      var arr;
      var match;
      var v;
      var _i;
      var _len;
      var _ref;
      var out;
      if (cond == null) {
        cond = {};
      }
      cond = $.extend({}, cond, {
        rel : rel
      });
      arr = function() {
        var _results;
        /** @type {Array} */
        _results = [];
        for (k in cond) {
          if (!__hasProp.call(cond, k)) {
            continue;
          }
          v = cond[k];
          _results.push(k);
        }
        return _results;
      }();
      _ref = this.data;
      /** @type {Array} */
      out = [];
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        d = _ref[_i];
        match = arr.reduce(function(dataAndEvents, k) {
          return dataAndEvents && d[k] === cond[k];
        }, true);
        if (match) {
          out.push(d);
        } else {

        }
      }
      return out;
    };
    return LinkParser;
  }();
  Annotator = Annotator || {};
  Annotator.Notification = function(_super) {
    /**
     * @param {?} data
     * @return {undefined}
     */
    function Notification(data) {
      this.hide = __bind(this.hide, this);
      this.show = __bind(this.show, this);
      Notification.__super__.constructor.call(this, $(this.options.html).appendTo(document.body)[0], data);
    }
    __extends(Notification, _super);
    Notification.prototype.events = {
      click : "hide"
    };
    Notification.prototype.options = {
      html : "<div class='annotator-notice'></div>",
      classes : {
        show : "annotator-notice-show",
        info : "annotator-notice-info",
        success : "annotator-notice-success",
        error : "annotator-notice-error"
      }
    };
    /**
     * @param {string} message
     * @param {string} status
     * @return {?}
     */
    Notification.prototype.show = function(message, status) {
      if (status == null) {
        status = Annotator.Notification.INFO;
      }
      /** @type {string} */
      this.currentStatus = status;
      $(this.element).addClass(this.options.classes.show).addClass(this.options.classes[this.currentStatus]).html(Util.escape(message || ""));
      setTimeout(this.hide, 5E3);
      return this;
    };
    /**
     * @return {?}
     */
    Notification.prototype.hide = function() {
      if (this.currentStatus == null) {
        this.currentStatus = Annotator.Notification.INFO;
      }
      $(this.element).removeClass(this.options.classes.show).removeClass(this.options.classes[this.currentStatus]);
      return this;
    };
    return Notification;
  }(Delegator);
  /** @type {string} */
  Annotator.Notification.INFO = "info";
  /** @type {string} */
  Annotator.Notification.SUCCESS = "success";
  /** @type {string} */
  Annotator.Notification.ERROR = "error";
  $(function() {
    var notification;
    notification = new Annotator.Notification;
    Annotator.showNotification = notification.show;
    return Annotator.hideNotification = notification.hide;
  });
  Annotator.Plugin.Unsupported = function(_super) {
    /**
     * @return {?}
     */
    function Unsupported() {
      _ref2 = Unsupported.__super__.constructor.apply(this, arguments);
      return _ref2;
    }
    __extends(Unsupported, _super);
    Unsupported.prototype.options = {
      message : Annotator._t("Sorry your current browser does not support the Annotator")
    };
    /**
     * @return {?}
     */
    Unsupported.prototype.pluginInit = function() {
      var _this = this;
      if (!Annotator.supported()) {
        return $(function() {
          Annotator.showNotification(_this.options.message);
          if (window.XMLHttpRequest === void 0 && ActiveXObject !== void 0) {
            return $("html").addClass("ie6");
          }
        });
      }
    };
    return Unsupported;
  }(Annotator.Plugin);
  /**
   * @param {string} string
   * @return {?}
   */
  createDateFromISO8601 = function(string) {
    var d;
    var date;
    var offset;
    var regexp;
    var time;
    var _ref3;
    /** @type {string} */
    regexp = "([0-9]{4})(-([0-9]{2})(-([0-9]{2})" + "(T([0-9]{2}):([0-9]{2})(:([0-9]{2})(\\.([0-9]+))?)?" + "(Z|(([-+])([0-9]{2}):([0-9]{2})))?)?)?)?";
    d = string.match(new RegExp(regexp));
    /** @type {number} */
    offset = 0;
    /** @type {Date} */
    date = new Date(d[1], 0, 1);
    if (d[3]) {
      date.setMonth(d[3] - 1);
    }
    if (d[5]) {
      date.setDate(d[5]);
    }
    if (d[7]) {
      date.setHours(d[7]);
    }
    if (d[8]) {
      date.setMinutes(d[8]);
    }
    if (d[10]) {
      date.setSeconds(d[10]);
    }
    if (d[12]) {
      date.setMilliseconds(Number("0." + d[12]) * 1E3);
    }
    if (d[14]) {
      /** @type {number} */
      offset = Number(d[16]) * 60 + Number(d[17]);
      offset *= (_ref3 = d[15] === "-") != null ? _ref3 : {
        1 : -1
      };
    }
    offset -= date.getTimezoneOffset();
    /** @type {number} */
    time = Number(date) + offset * 60 * 1E3;
    date.setTime(Number(time));
    return date;
  };
  /**
   * @param {string} data
   * @return {?}
   */
  base64Decode = function(data) {
    var ac;
    var excludes;
    var bits;
    var expires;
    var o1;
    var o2;
    var h3;
    var h4;
    var i;
    var lo;
    var surrogate2;
    var o3;
    var tmp_arr;
    if (typeof atob !== "undefined" && atob !== null) {
      return atob(data);
    } else {
      /** @type {string} */
      excludes = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
      /** @type {number} */
      i = 0;
      /** @type {number} */
      ac = 0;
      /** @type {string} */
      expires = "";
      /** @type {Array} */
      tmp_arr = [];
      if (!data) {
        return data;
      }
      data += "";
      for (;i < data.length;) {
        /** @type {number} */
        o1 = excludes.indexOf(data.charAt(i++));
        /** @type {number} */
        o2 = excludes.indexOf(data.charAt(i++));
        /** @type {number} */
        h3 = excludes.indexOf(data.charAt(i++));
        /** @type {number} */
        h4 = excludes.indexOf(data.charAt(i++));
        /** @type {number} */
        bits = o1 << 18 | o2 << 12 | h3 << 6 | h4;
        /** @type {number} */
        lo = bits >> 16 & 255;
        /** @type {number} */
        surrogate2 = bits >> 8 & 255;
        /** @type {number} */
        o3 = bits & 255;
        if (h3 === 64) {
          /** @type {string} */
          tmp_arr[ac++] = String.fromCharCode(lo);
        } else {
          if (h4 === 64) {
            /** @type {string} */
            tmp_arr[ac++] = String.fromCharCode(lo, surrogate2);
          } else {
            /** @type {string} */
            tmp_arr[ac++] = String.fromCharCode(lo, surrogate2, o3);
          }
        }
      }
      return tmp_arr.join("");
    }
  };
  /**
   * @param {string} data
   * @return {?}
   */
  base64UrlDecode = function(data) {
    var i;
    var m;
    var _i;
    var _ref;
    /** @type {number} */
    m = data.length % 4;
    if (m !== 0) {
      /** @type {number} */
      i = _i = 0;
      /** @type {number} */
      _ref = 4 - m;
      for (;0 <= _ref ? _i < _ref : _i > _ref;i = 0 <= _ref ? ++_i : --_i) {
        data += "=";
      }
    }
    data = data.replace(/-/g, "+");
    data = data.replace(/_/g, "/");
    return base64Decode(data);
  };
  /**
   * @param {string} token
   * @return {?}
   */
  parseToken = function(token) {
    var val;
    var payload;
    var elementListeners;
    var _ref;
    _ref = token.split(".");
    val = _ref[0];
    payload = _ref[1];
    elementListeners = _ref[2];
    return JSON.parse(base64UrlDecode(payload));
  };
  Annotator.Plugin.Auth = function(_super) {
    /**
     * @param {?} $cookieStore
     * @param {?} $location
     * @return {undefined}
     */
    function Auth($cookieStore, $location) {
      Auth.__super__.constructor.apply(this, arguments);
      /** @type {Array} */
      this.waitingForToken = [];
      if (this.options.token) {
        this.setToken(this.options.token);
      } else {
        this.requestToken();
      }
    }
    __extends(Auth, _super);
    Auth.prototype.options = {
      token : null,
      tokenUrl : "/auth/token",
      autoFetch : true
    };
    /**
     * @return {?}
     */
    Auth.prototype.requestToken = function() {
      var _this = this;
      /** @type {boolean} */
      this.requestInProgress = true;
      return $.ajax({
        url : this.options.tokenUrl,
        dataType : "text",
        xhrFields : {
          withCredentials : true
        }
      }).done(function(token, dataAndEvents, deepDataAndEvents) {
        return _this.setToken(token);
      }).fail(function(m, dataAndEvents, err) {
        var res;
        res = Annotator._t("Couldn't get auth token:");
        console.error("" + res + " " + err, m);
        return Annotator.showNotification("" + res + " " + m.responseText, Annotator.Notification.ERROR);
      }).always(function() {
        return _this.requestInProgress = false;
      });
    };
    /**
     * @param {string} token
     * @return {?}
     */
    Auth.prototype.setToken = function(token) {
      var _results;
      var _this = this;
      /** @type {string} */
      this.token = token;
      this._unsafeToken = parseToken(token);
      if (this.haveValidToken()) {
        if (this.options.autoFetch) {
          /** @type {number} */
          this.refreshTimeout = setTimeout(function() {
            return _this.requestToken();
          }, (this.timeToExpiry() - 2) * 1E3);
        }
        this.updateHeaders();
        /** @type {Array} */
        _results = [];
        for (;this.waitingForToken.length > 0;) {
          _results.push(this.waitingForToken.pop()(this._unsafeToken));
        }
        return _results;
      } else {
        console.warn(Annotator._t("Didn't get a valid token."));
        if (this.options.autoFetch) {
          console.warn(Annotator._t("Getting a new token in 10s."));
          return setTimeout(function() {
            return _this.requestToken();
          }, 10 * 1E3);
        }
      }
    };
    /**
     * @return {?}
     */
    Auth.prototype.haveValidToken = function() {
      var allFields;
      allFields = this._unsafeToken && (this._unsafeToken.issuedAt && (this._unsafeToken.ttl && this._unsafeToken.consumerKey));
      if (allFields && this.timeToExpiry() > 0) {
        return true;
      } else {
        return false;
      }
    };
    /**
     * @return {?}
     */
    Auth.prototype.timeToExpiry = function() {
      var expiry;
      var issue;
      var now;
      var timeToExpiry;
      /** @type {number} */
      now = (new Date).getTime() / 1E3;
      /** @type {number} */
      issue = createDateFromISO8601(this._unsafeToken.issuedAt).getTime() / 1E3;
      expiry = issue + this._unsafeToken.ttl;
      /** @type {number} */
      timeToExpiry = expiry - now;
      if (timeToExpiry > 0) {
        return timeToExpiry;
      } else {
        return 0;
      }
    };
    /**
     * @return {?}
     */
    Auth.prototype.updateHeaders = function() {
      var current;
      current = this.element.data("annotator:headers");
      return this.element.data("annotator:headers", $.extend(current, {
        "x-annotator-auth-token" : this.token
      }));
    };
    /**
     * @param {(Object|boolean|number|string)} callback
     * @return {?}
     */
    Auth.prototype.withToken = function(callback) {
      if (callback == null) {
        return;
      }
      if (this.haveValidToken()) {
        return callback(this._unsafeToken);
      } else {
        this.waitingForToken.push(callback);
        if (!this.requestInProgress) {
          return this.requestToken();
        }
      }
    };
    return Auth;
  }(Annotator.Plugin);
  Annotator.Plugin.Store = function(_super) {
    /**
     * @param {?} name
     * @param {?} options
     * @return {undefined}
     */
    function Store(name, options) {
      this._onError = __bind(this._onError, this);
      this._onLoadAnnotationsFromSearch = __bind(this._onLoadAnnotationsFromSearch, this);
      this._onLoadAnnotations = __bind(this._onLoadAnnotations, this);
      this._getAnnotations = __bind(this._getAnnotations, this);
      Store.__super__.constructor.apply(this, arguments);
      /** @type {Array} */
      this.annotations = [];
    }
    __extends(Store, _super);
    Store.prototype.events = {
      annotationCreated : "annotationCreated",
      annotationDeleted : "annotationDeleted",
      annotationUpdated : "annotationUpdated"
    };
    Store.prototype.options = {
      annotationData : {},
      emulateHTTP : false,
      loadFromSearch : false,
      prefix : "/store",
      urls : {
        create : "/annotations",
        read : "/annotations/:id",
        update : "/annotations/:id",
        destroy : "/annotations/:id",
        search : "/search"
      }
    };
    /**
     * @return {?}
     */
    Store.prototype.pluginInit = function() {
      if (!Annotator.supported()) {
        return;
      }
      if (this.annotator.plugins.Auth) {
        return this.annotator.plugins.Auth.withToken(this._getAnnotations);
      } else {
        return this._getAnnotations();
      }
    };
    /**
     * @return {?}
     */
    Store.prototype._getAnnotations = function() {
      if (this.options.loadFromSearch) {
        return this.loadAnnotationsFromSearch(this.options.loadFromSearch);
      } else {
        return this.loadAnnotations();
      }
    };
    /**
     * @param {string} annotation
     * @return {?}
     */
    Store.prototype.annotationCreated = function(annotation) {
      var _this = this;
      if (__indexOf.call(this.annotations, annotation) < 0) {
        this.registerAnnotation(annotation);
        return this._apiRequest("create", annotation, function(data) {
          if (data.id == null) {
            console.warn(Annotator._t("Warning: No ID returned from server for annotation "), annotation);
          }
          return _this.updateAnnotation(annotation, data);
        });
      } else {
        return this.updateAnnotation(annotation, {});
      }
    };
    /**
     * @param {string} annotation
     * @return {?}
     */
    Store.prototype.annotationUpdated = function(annotation) {
      var _this = this;
      if (__indexOf.call(this.annotations, annotation) >= 0) {
        return this._apiRequest("update", annotation, function(data) {
          return _this.updateAnnotation(annotation, data);
        });
      }
    };
    /**
     * @param {string} annotation
     * @return {?}
     */
    Store.prototype.annotationDeleted = function(annotation) {
      var _this = this;
      if (__indexOf.call(this.annotations, annotation) >= 0) {
        return this._apiRequest("destroy", annotation, function() {
          return _this.unregisterAnnotation(annotation);
        });
      }
    };
    /**
     * @param {string} annotation
     * @return {?}
     */
    Store.prototype.registerAnnotation = function(annotation) {
      return this.annotations.push(annotation);
    };
    /**
     * @param {string} annotation
     * @return {?}
     */
    Store.prototype.unregisterAnnotation = function(annotation) {
      return this.annotations.splice(this.annotations.indexOf(annotation), 1);
    };
    /**
     * @param {string} annotation
     * @param {?} data
     * @return {?}
     */
    Store.prototype.updateAnnotation = function(annotation, data) {
      if (__indexOf.call(this.annotations, annotation) < 0) {
        console.error(Annotator._t("Trying to update unregistered annotation!"));
      } else {
        $.extend(annotation, data);
      }
      return $(annotation.highlights).data("annotation", annotation);
    };
    /**
     * @return {?}
     */
    Store.prototype.loadAnnotations = function() {
      return this._apiRequest("read", null, this._onLoadAnnotations);
    };
    /**
     * @param {Array} worlds
     * @return {?}
     */
    Store.prototype._onLoadAnnotations = function(worlds) {
      var a;
      var annotation;
      var annotationMap;
      var newData;
      var _i;
      var i;
      var _len;
      var max;
      var _ref;
      if (worlds == null) {
        /** @type {Array} */
        worlds = [];
      }
      annotationMap = {};
      _ref = this.annotations;
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        a = _ref[_i];
        annotationMap[a.id] = a;
      }
      /** @type {Array} */
      newData = [];
      /** @type {number} */
      i = 0;
      max = worlds.length;
      for (;i < max;i++) {
        a = worlds[i];
        if (annotationMap[a.id]) {
          annotation = annotationMap[a.id];
          this.updateAnnotation(annotation, a);
        } else {
          newData.push(a);
        }
      }
      this.annotations = this.annotations.concat(newData);
      return this.annotator.loadAnnotations(newData.slice());
    };
    /**
     * @param {string} searchOptions
     * @return {?}
     */
    Store.prototype.loadAnnotationsFromSearch = function(searchOptions) {
      return this._apiRequest("search", searchOptions, this._onLoadAnnotationsFromSearch);
    };
    /**
     * @param {Object} data
     * @return {?}
     */
    Store.prototype._onLoadAnnotationsFromSearch = function(data) {
      if (data == null) {
        data = {};
      }
      return this._onLoadAnnotations(data.rows || []);
    };
    /**
     * @return {?}
     */
    Store.prototype.dumpAnnotations = function() {
      var ann;
      var _i;
      var _len;
      var _ref;
      var _results;
      _ref = this.annotations;
      /** @type {Array} */
      _results = [];
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        ann = _ref[_i];
        _results.push(JSON.parse(this._dataFor(ann)));
      }
      return _results;
    };
    /**
     * @param {string} action
     * @param {string} obj
     * @param {Function} onSuccess
     * @return {?}
     */
    Store.prototype._apiRequest = function(action, obj, onSuccess) {
      var id;
      var options;
      var request;
      var url;
      id = obj && obj.id;
      url = this._urlFor(action, id);
      options = this._apiRequestOptions(action, obj, onSuccess);
      request = $.ajax(url, options);
      request._id = id;
      /** @type {string} */
      request._action = action;
      return request;
    };
    /**
     * @param {string} action
     * @param {Object} obj
     * @param {Function} onSuccess
     * @return {?}
     */
    Store.prototype._apiRequestOptions = function(action, obj, onSuccess) {
      var data;
      var method;
      var opts;
      method = this._methodFor(action);
      opts = {
        type : method,
        headers : this.element.data("annotator:headers"),
        dataType : "json",
        success : onSuccess || function() {
        },
        error : this._onError
      };
      if (this.options.emulateHTTP && (method === "PUT" || method === "DELETE")) {
        opts.headers = $.extend(opts.headers, {
          "X-HTTP-Method-Override" : method
        });
        /** @type {string} */
        opts.type = "POST";
      }
      if (action === "search") {
        opts = $.extend(opts, {
          data : obj
        });
        return opts;
      }
      data = obj && this._dataFor(obj);
      if (this.options.emulateJSON) {
        opts.data = {
          json : data
        };
        if (this.options.emulateHTTP) {
          opts.data._method = method;
        }
        return opts;
      }
      opts = $.extend(opts, {
        data : data,
        contentType : "application/json; charset=utf-8"
      });
      return opts;
    };
    /**
     * @param {string} action
     * @param {string} id
     * @return {?}
     */
    Store.prototype._urlFor = function(action, id) {
      var url;
      url = this.options.prefix != null ? this.options.prefix : "";
      url += this.options.urls[action];
      url = url.replace(/\/:id/, id != null ? "/" + id : "");
      url = url.replace(/:id/, id != null ? id : "");
      return url;
    };
    /**
     * @param {string} action
     * @return {?}
     */
    Store.prototype._methodFor = function(action) {
      var table;
      table = {
        create : "POST",
        read : "GET",
        update : "PUT",
        destroy : "DELETE",
        search : "GET"
      };
      return table[action];
    };
    /**
     * @param {Object} annotation
     * @return {?}
     */
    Store.prototype._dataFor = function(annotation) {
      var data;
      var highlights;
      highlights = annotation.highlights;
      delete annotation.highlights;
      $.extend(annotation, this.options.annotationData);
      /** @type {string} */
      data = JSON.stringify(annotation);
      if (highlights) {
        annotation.highlights = highlights;
      }
      return data;
    };
    /**
     * @param {Object} xhr
     * @return {?}
     */
    Store.prototype._onError = function(xhr) {
      var action;
      var message;
      action = xhr._action;
      message = Annotator._t("Sorry we could not ") + action + Annotator._t(" this annotation");
      if (xhr._action === "search") {
        message = Annotator._t("Sorry we could not search the store for annotations");
      } else {
        if (xhr._action === "read" && !xhr._id) {
          message = Annotator._t("Sorry we could not ") + action + Annotator._t(" the annotations from the store");
        }
      }
      switch(xhr.status) {
        case 401:
          message = Annotator._t("Sorry you are not allowed to ") + action + Annotator._t(" this annotation");
          break;
        case 404:
          message = Annotator._t("Sorry we could not connect to the annotations store");
          break;
        case 500:
          message = Annotator._t("Sorry something went wrong with the annotation store");
      }
      Annotator.showNotification(message, Annotator.Notification.ERROR);
      return console.error(Annotator._t("API request failed:") + (" '" + xhr.status + "'"));
    };
    return Store;
  }(Annotator.Plugin);
  Annotator.Plugin.Permissions = function(_super) {
    /**
     * @param {?} element
     * @param {?} dataAndEvents
     * @return {undefined}
     */
    function Permissions(element, dataAndEvents) {
      this._setAuthFromToken = __bind(this._setAuthFromToken, this);
      this.updateViewer = __bind(this.updateViewer, this);
      this.updateAnnotationPermissions = __bind(this.updateAnnotationPermissions, this);
      this.updatePermissionsField = __bind(this.updatePermissionsField, this);
      this.addFieldsToAnnotation = __bind(this.addFieldsToAnnotation, this);
      Permissions.__super__.constructor.apply(this, arguments);
      if (this.options.user) {
        this.setUser(this.options.user);
        delete this.options.user;
      }
    }
    __extends(Permissions, _super);
    Permissions.prototype.events = {
      beforeAnnotationCreated : "addFieldsToAnnotation"
    };
    Permissions.prototype.options = {
      showViewPermissionsCheckbox : true,
      showEditPermissionsCheckbox : true,
      /**
       * @param {string} user
       * @return {?}
       */
      userId : function(user) {
        return user;
      },
      /**
       * @param {string} user
       * @return {?}
       */
      userString : function(user) {
        return user;
      },
      /**
       * @param {?} action
       * @param {Object} annotation
       * @param {string} user
       * @return {?}
       */
      userAuthorize : function(action, annotation, user) {
        var token;
        var _ref;
        var _i;
        var _len;
        if (annotation.permissions) {
          _ref = annotation.permissions[action] || [];
          if (_ref.length === 0) {
            return true;
          }
          /** @type {number} */
          _i = 0;
          _len = _ref.length;
          for (;_i < _len;_i++) {
            token = _ref[_i];
            if (this.userId(user) === token) {
              return true;
            }
          }
          return false;
        } else {
          if (annotation.user) {
            if (user) {
              return this.userId(user) === this.userId(annotation.user);
            } else {
              return false;
            }
          }
        }
        return true;
      },
      user : "",
      permissions : {
        read : [],
        update : [],
        "delete" : [],
        admin : []
      }
    };
    /**
     * @return {?}
     */
    Permissions.prototype.pluginInit = function() {
      var createCallback;
      var self;
      var _this = this;
      if (!Annotator.supported()) {
        return;
      }
      self = this;
      /**
       * @param {string} method
       * @param {string} type
       * @return {?}
       */
      createCallback = function(method, type) {
        return function(graphics, capture) {
          return self[method].call(self, type, graphics, capture);
        };
      };
      if (!this.user && this.annotator.plugins.Auth) {
        this.annotator.plugins.Auth.withToken(this._setAuthFromToken);
      }
      if (this.options.showViewPermissionsCheckbox === true) {
        this.annotator.editor.addField({
          type : "checkbox",
          label : Annotator._t("Allow anyone to <strong>view</strong> this annotation"),
          load : createCallback("updatePermissionsField", "read"),
          submit : createCallback("updateAnnotationPermissions", "read")
        });
      }
      if (this.options.showEditPermissionsCheckbox === true) {
        this.annotator.editor.addField({
          type : "checkbox",
          label : Annotator._t("Allow anyone to <strong>edit</strong> this annotation"),
          load : createCallback("updatePermissionsField", "update"),
          submit : createCallback("updateAnnotationPermissions", "update")
        });
      }
      this.annotator.viewer.addField({
        load : this.updateViewer
      });
      if (this.annotator.plugins.Filter) {
        return this.annotator.plugins.Filter.addFilter({
          label : Annotator._t("User"),
          property : "user",
          /**
           * @param {string} input
           * @param {string} user
           * @return {?}
           */
          isFiltered : function(input, user) {
            var name;
            var i;
            var ln;
            var configList;
            user = _this.options.userString(user);
            if (!(input && user)) {
              return false;
            }
            configList = input.split(/\s*/);
            /** @type {number} */
            i = 0;
            ln = configList.length;
            for (;i < ln;i++) {
              name = configList[i];
              if (user.indexOf(name) === -1) {
                return false;
              }
            }
            return true;
          }
        });
      }
    };
    /**
     * @param {string} user
     * @return {?}
     */
    Permissions.prototype.setUser = function(user) {
      return this.user = user;
    };
    /**
     * @param {Object} annotation
     * @return {?}
     */
    Permissions.prototype.addFieldsToAnnotation = function(annotation) {
      if (annotation) {
        annotation.permissions = $.extend(true, {}, this.options.permissions);
        if (this.user) {
          return annotation.user = this.user;
        }
      }
    };
    /**
     * @param {string} action
     * @param {Object} annotation
     * @param {string} user
     * @return {?}
     */
    Permissions.prototype.authorize = function(action, annotation, user) {
      if (user === void 0) {
        user = this.user;
      }
      if (this.options.userAuthorize) {
        return this.options.userAuthorize.call(this.options, action, annotation, user);
      } else {
        return true;
      }
    };
    /**
     * @param {string} action
     * @param {Object} field
     * @param {Object} annotation
     * @return {?}
     */
    Permissions.prototype.updatePermissionsField = function(action, field, annotation) {
      var input;
      field = $(field).show();
      input = field.find("input").removeAttr("disabled");
      if (!this.authorize("admin", annotation)) {
        field.hide();
      }
      if (this.authorize(action, annotation || {}, null)) {
        return input.attr("checked", "checked");
      } else {
        return input.removeAttr("checked");
      }
    };
    /**
     * @param {string} type
     * @param {?} field
     * @param {Object} annotation
     * @return {?}
     */
    Permissions.prototype.updateAnnotationPermissions = function(type, field, annotation) {
      var dataKey;
      if (!annotation.permissions) {
        annotation.permissions = $.extend(true, {}, this.options.permissions);
      }
      /** @type {string} */
      dataKey = type + "-permissions";
      if ($(field).find("input").is(":checked")) {
        return annotation.permissions[type] = [];
      } else {
        return annotation.permissions[type] = [this.options.userId(this.user)];
      }
    };
    /**
     * @param {Object} field
     * @param {Object} annotation
     * @param {?} controls
     * @return {?}
     */
    Permissions.prototype.updateViewer = function(field, annotation, controls) {
      var user;
      var username;
      field = $(field);
      username = this.options.userString(annotation.user);
      if (annotation.user && (username && typeof username === "string")) {
        user = Annotator.Util.escape(this.options.userString(annotation.user));
        field.html(user).addClass("annotator-user");
      } else {
        field.remove();
      }
      if (controls) {
        if (!this.authorize("update", annotation)) {
          controls.hideEdit();
        }
        if (!this.authorize("delete", annotation)) {
          return controls.hideDelete();
        }
      }
    };
    /**
     * @param {Object} token
     * @return {?}
     */
    Permissions.prototype._setAuthFromToken = function(token) {
      return this.setUser(token.userId);
    };
    return Permissions;
  }(Annotator.Plugin);
  Annotator.Plugin.AnnotateItPermissions = function(_super) {
    /**
     * @return {?}
     */
    function AnnotateItPermissions() {
      this._setAuthFromToken = __bind(this._setAuthFromToken, this);
      this.updateAnnotationPermissions = __bind(this.updateAnnotationPermissions, this);
      this.updatePermissionsField = __bind(this.updatePermissionsField, this);
      this.addFieldsToAnnotation = __bind(this.addFieldsToAnnotation, this);
      _ref3 = AnnotateItPermissions.__super__.constructor.apply(this, arguments);
      return _ref3;
    }
    __extends(AnnotateItPermissions, _super);
    AnnotateItPermissions.prototype.options = {
      showViewPermissionsCheckbox : true,
      showEditPermissionsCheckbox : true,
      groups : {
        world : "group:__world__",
        authenticated : "group:__authenticated__",
        consumer : "group:__consumer__"
      },
      /**
       * @param {Object} user
       * @return {?}
       */
      userId : function(user) {
        return user.userId;
      },
      /**
       * @param {Object} user
       * @return {?}
       */
      userString : function(user) {
        return user.userId;
      },
      /**
       * @param {string} off
       * @param {Object} annotation
       * @param {Object} user
       * @return {?}
       */
      userAuthorize : function(off, annotation, user) {
        var uniqs;
        var buf;
        var _ref1;
        var _ref2;
        var _ref6;
        var _ref4;
        buf = annotation.permissions || {};
        uniqs = buf[off] || [];
        if (_ref1 = this.groups.world, __indexOf.call(uniqs, _ref1) >= 0) {
          return true;
        } else {
          if (user != null && (user.userId != null && user.consumerKey != null)) {
            if (user.userId === annotation.user && user.consumerKey === annotation.consumer) {
              return true;
            } else {
              if (_ref2 = this.groups.authenticated, __indexOf.call(uniqs, _ref2) >= 0) {
                return true;
              } else {
                if (user.consumerKey === annotation.consumer && (_ref6 = this.groups.consumer, __indexOf.call(uniqs, _ref6) >= 0)) {
                  return true;
                } else {
                  if (user.consumerKey === annotation.consumer && (_ref4 = user.userId, __indexOf.call(uniqs, _ref4) >= 0)) {
                    return true;
                  } else {
                    if (user.consumerKey === annotation.consumer && user.admin) {
                      return true;
                    } else {
                      return false;
                    }
                  }
                }
              }
            }
          } else {
            return false;
          }
        }
      },
      permissions : {
        read : ["group:__world__"],
        update : [],
        "delete" : [],
        admin : []
      }
    };
    /**
     * @param {Object} annotation
     * @return {?}
     */
    AnnotateItPermissions.prototype.addFieldsToAnnotation = function(annotation) {
      if (annotation) {
        annotation.permissions = this.options.permissions;
        if (this.user) {
          annotation.user = this.user.userId;
          return annotation.consumer = this.user.consumerKey;
        }
      }
    };
    /**
     * @param {string} action
     * @param {Object} field
     * @param {Object} annotation
     * @return {?}
     */
    AnnotateItPermissions.prototype.updatePermissionsField = function(action, field, annotation) {
      var input;
      field = $(field).show();
      input = field.find("input").removeAttr("disabled");
      if (!this.authorize("admin", annotation)) {
        field.hide();
      }
      if (this.user && this.authorize(action, annotation || {}, {
        userId : "__nonexistentuser__",
        consumerKey : this.user.consumerKey
      })) {
        return input.attr("checked", "checked");
      } else {
        return input.removeAttr("checked");
      }
    };
    /**
     * @param {string} type
     * @param {?} field
     * @param {Object} annotation
     * @return {?}
     */
    AnnotateItPermissions.prototype.updateAnnotationPermissions = function(type, field, annotation) {
      var dataKey;
      if (!annotation.permissions) {
        annotation.permissions = this.options.permissions;
      }
      /** @type {string} */
      dataKey = type + "-permissions";
      if ($(field).find("input").is(":checked")) {
        return annotation.permissions[type] = [type === "read" ? this.options.groups.world : this.options.groups.consumer];
      } else {
        return annotation.permissions[type] = [];
      }
    };
    /**
     * @param {string} token
     * @return {?}
     */
    AnnotateItPermissions.prototype._setAuthFromToken = function(token) {
      return this.setUser(token);
    };
    return AnnotateItPermissions;
  }(Annotator.Plugin.Permissions);
  Annotator.Plugin.Filter = function(_super) {
    /**
     * @param {Text} filters
     * @param {Object} options
     * @return {undefined}
     */
    function Filter(filters, options) {
      this._onPreviousClick = __bind(this._onPreviousClick, this);
      this._onNextClick = __bind(this._onNextClick, this);
      this._onFilterKeyup = __bind(this._onFilterKeyup, this);
      this._onFilterBlur = __bind(this._onFilterBlur, this);
      this._onFilterFocus = __bind(this._onFilterFocus, this);
      this.updateHighlights = __bind(this.updateHighlights, this);
      var _base;
      filters = $(this.html.element).appendTo((options != null ? options.appendTo : void 0) || this.options.appendTo);
      Filter.__super__.constructor.call(this, filters, options);
      if (!(_base = this.options).filters) {
        /** @type {Array} */
        _base.filters = [];
      }
      this.filter = $(this.html.filter);
      /** @type {Array} */
      this.filters = [];
      /** @type {number} */
      this.current = 0;
    }
    __extends(Filter, _super);
    Filter.prototype.events = {
      ".annotator-filter-property input focus" : "_onFilterFocus",
      ".annotator-filter-property input blur" : "_onFilterBlur",
      ".annotator-filter-property input keyup" : "_onFilterKeyup",
      ".annotator-filter-previous click" : "_onPreviousClick",
      ".annotator-filter-next click" : "_onNextClick",
      ".annotator-filter-clear click" : "_onClearClick"
    };
    Filter.prototype.classes = {
      active : "annotator-filter-active",
      hl : {
        hide : "annotator-hl-filtered",
        active : "annotator-hl-active"
      }
    };
    Filter.prototype.html = {
      element : '<div class="annotator-filter">\n  <strong>' + Annotator._t("Navigate:") + '</strong>\n<span class="annotator-filter-navigation">\n  <button class="annotator-filter-previous">' + Annotator._t("Previous") + '</button>\n<button class="annotator-filter-next">' + Annotator._t("Next") + "</button>\n</span>\n<strong>" + Annotator._t("Filter by:") + "</strong>\n</div>",
      filter : '<span class="annotator-filter-property">\n  <label></label>\n  <input/>\n  <button class="annotator-filter-clear">' + Annotator._t("Clear") + "</button>\n</span>"
    };
    Filter.prototype.options = {
      appendTo : "body",
      filters : [],
      addAnnotationFilter : true,
      /**
       * @param {string} input
       * @param {string} property
       * @return {?}
       */
      isFiltered : function(input, property) {
        var x;
        var _i;
        var _len;
        var xs;
        if (!(input && property)) {
          return false;
        }
        xs = input.split(/\s+/);
        /** @type {number} */
        _i = 0;
        _len = xs.length;
        for (;_i < _len;_i++) {
          x = xs[_i];
          if (property.indexOf(x) === -1) {
            return false;
          }
        }
        return true;
      }
    };
    /**
     * @return {?}
     */
    Filter.prototype.pluginInit = function() {
      var filter;
      var _i;
      var _len;
      var _ref;
      _ref = this.options.filters;
      /** @type {number} */
      _i = 0;
      _len = _ref.length;
      for (;_i < _len;_i++) {
        filter = _ref[_i];
        this.addFilter(filter);
      }
      this.updateHighlights();
      this._setupListeners()._insertSpacer();
      if (this.options.addAnnotationFilter === true) {
        return this.addFilter({
          label : Annotator._t("Annotation"),
          property : "text"
        });
      }
    };
    /**
     * @return {?}
     */
    Filter.prototype.destroy = function() {
      var currentMargin;
      var html;
      Filter.__super__.destroy.apply(this, arguments);
      html = $("html");
      /** @type {number} */
      currentMargin = parseInt(html.css("padding-top"), 10) || 0;
      html.css("padding-top", currentMargin - this.element.outerHeight());
      return this.element.remove();
    };
    /**
     * @return {?}
     */
    Filter.prototype._insertSpacer = function() {
      var value;
      var element;
      element = $("html");
      /** @type {number} */
      value = parseInt(element.css("padding-top"), 10) || 0;
      element.css("padding-top", value + this.element.outerHeight());
      return this;
    };
    /**
     * @return {?}
     */
    Filter.prototype._setupListeners = function() {
      var event;
      var events;
      var _i;
      var _len;
      /** @type {Array} */
      events = ["annotationsLoaded", "annotationCreated", "annotationUpdated", "annotationDeleted"];
      /** @type {number} */
      _i = 0;
      /** @type {number} */
      _len = events.length;
      for (;_i < _len;_i++) {
        event = events[_i];
        this.annotator.subscribe(event, this.updateHighlights);
      }
      return this;
    };
    /**
     * @param {?} options
     * @return {?}
     */
    Filter.prototype.addFilter = function(options) {
      var f;
      var filter;
      filter = $.extend({
        label : "",
        property : "",
        isFiltered : this.options.isFiltered
      }, options);
      if (!function() {
        var _i;
        var _len;
        var _ref;
        var _results;
        _ref = this.filters;
        /** @type {Array} */
        _results = [];
        /** @type {number} */
        _i = 0;
        _len = _ref.length;
        for (;_i < _len;_i++) {
          f = _ref[_i];
          if (f.property === filter.property) {
            _results.push(f);
          }
        }
        return _results;
      }.call(this).length) {
        /** @type {string} */
        filter.id = "annotator-filter-" + filter.property;
        /** @type {Array} */
        filter.annotations = [];
        filter.element = this.filter.clone().appendTo(this.element);
        filter.element.find("label").html(filter.label).attr("for", filter.id);
        filter.element.find("input").attr({
          id : filter.id,
          placeholder : Annotator._t("Filter by ") + filter.label + "\u2026"
        });
        filter.element.find("button").hide();
        filter.element.data("filter", filter);
        this.filters.push(filter);
      }
      return this;
    };
    /**
     * @param {Object} filter
     * @return {?}
     */
    Filter.prototype.updateFilter = function(filter) {
      var annotation;
      var elements;
      var input;
      var property;
      var i;
      var valsLength;
      var codeSegments;
      /** @type {Array} */
      filter.annotations = [];
      this.updateHighlights();
      this.resetHighlights();
      input = $.trim(filter.element.find("input").val());
      if (input) {
        elements = this.highlights.map(function() {
          return $(this).data("annotation");
        });
        codeSegments = $.makeArray(elements);
        /** @type {number} */
        i = 0;
        valsLength = codeSegments.length;
        for (;i < valsLength;i++) {
          annotation = codeSegments[i];
          property = annotation[filter.property];
          if (filter.isFiltered(input, property)) {
            filter.annotations.push(annotation);
          }
        }
        return this.filterHighlights();
      }
    };
    /**
     * @return {?}
     */
    Filter.prototype.updateHighlights = function() {
      this.highlights = this.annotator.element.find(".annotator-hl:visible");
      return this.filtered = this.highlights.not(this.classes.hl.hide);
    };
    /**
     * @return {?}
     */
    Filter.prototype.filterHighlights = function() {
      var activeFilters;
      var annotation;
      var scripts;
      var filtered;
      var highlights;
      var index;
      var elems;
      var _i;
      var _len;
      var _ref4;
      activeFilters = $.grep(this.filters, function(store) {
        return!!store.annotations.length;
      });
      filtered = ((_ref4 = activeFilters[0]) != null ? _ref4.annotations : void 0) || [];
      if (activeFilters.length > 1) {
        /** @type {Array} */
        scripts = [];
        $.each(activeFilters, function() {
          return $.merge(scripts, this.annotations);
        });
        /** @type {Array} */
        elems = [];
        /** @type {Array} */
        filtered = [];
        $.each(scripts, function() {
          if ($.inArray(this, elems) === -1) {
            return elems.push(this);
          } else {
            return filtered.push(this);
          }
        });
      }
      highlights = this.highlights;
      /** @type {number} */
      index = _i = 0;
      _len = filtered.length;
      for (;_i < _len;index = ++_i) {
        annotation = filtered[index];
        highlights = highlights.not(annotation.highlights);
      }
      highlights.addClass(this.classes.hl.hide);
      this.filtered = this.highlights.not(this.classes.hl.hide);
      return this;
    };
    /**
     * @return {?}
     */
    Filter.prototype.resetHighlights = function() {
      this.highlights.removeClass(this.classes.hl.hide);
      this.filtered = this.highlights;
      return this;
    };
    /**
     * @param {Event} event
     * @return {?}
     */
    Filter.prototype._onFilterFocus = function(event) {
      var element;
      element = $(event.target);
      element.parent().addClass(this.classes.active);
      return element.next("button").show();
    };
    /**
     * @param {Event} event
     * @return {?}
     */
    Filter.prototype._onFilterBlur = function(event) {
      var element;
      if (!event.target.value) {
        element = $(event.target);
        element.parent().removeClass(this.classes.active);
        return element.next("button").hide();
      }
    };
    /**
     * @param {Event} event
     * @return {?}
     */
    Filter.prototype._onFilterKeyup = function(event) {
      var filter;
      filter = $(event.target).parent().data("filter");
      if (filter) {
        return this.updateFilter(filter);
      }
    };
    /**
     * @param {boolean} previous
     * @return {?}
     */
    Filter.prototype._findNextHighlight = function(previous) {
      var self;
      var annotation;
      var current;
      var index;
      var next;
      var offset;
      var operator;
      var curr;
      if (!this.highlights.length) {
        return this;
      }
      /** @type {number} */
      offset = previous ? 0 : -1;
      /** @type {number} */
      curr = previous ? -1 : 0;
      /** @type {string} */
      operator = previous ? "lt" : "gt";
      self = this.highlights.not("." + this.classes.hl.hide);
      current = self.filter("." + this.classes.hl.active);
      if (!current.length) {
        current = self.eq(offset);
      }
      annotation = current.data("annotation");
      index = self.index(current[0]);
      next = self.filter(":" + operator + "(" + index + ")").not(annotation.highlights).eq(curr);
      if (!next.length) {
        next = self.eq(curr);
      }
      return this._scrollToHighlight(next.data("annotation").highlights);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    Filter.prototype._onNextClick = function(event) {
      return this._findNextHighlight();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    Filter.prototype._onPreviousClick = function(event) {
      return this._findNextHighlight(true);
    };
    /**
     * @param {Element} highlight
     * @return {?}
     */
    Filter.prototype._scrollToHighlight = function(highlight) {
      highlight = $(highlight);
      this.highlights.removeClass(this.classes.hl.active);
      highlight.addClass(this.classes.hl.active);
      return $("html, body").animate({
        scrollTop : highlight.offset().top - (this.element.height() + 20)
      }, 150);
    };
    /**
     * @param {Event} e
     * @return {?}
     */
    Filter.prototype._onClearClick = function(e) {
      return $(e.target).prev("input").val("").keyup().blur();
    };
    return Filter;
  }(Annotator.Plugin);
  Annotator.Plugin.Markdown = function(_super) {
    /**
     * @param {?} stripUnwanted
     * @param {?} dialect
     * @return {undefined}
     */
    function Markdown(stripUnwanted, dialect) {
      this.updateTextField = __bind(this.updateTextField, this);
      if ((typeof Showdown !== "undefined" && Showdown !== null ? Showdown.converter : void 0) != null) {
        Markdown.__super__.constructor.apply(this, arguments);
        this.converter = new Showdown.converter;
      } else {
        console.error(Annotator._t("To use the Markdown plugin, you must include Showdown into the page first."));
      }
    }
    __extends(Markdown, _super);
    Markdown.prototype.events = {
      annotationViewerTextField : "updateTextField"
    };
    /**
     * @param {?} field
     * @param {Object} annotation
     * @return {?}
     */
    Markdown.prototype.updateTextField = function(field, annotation) {
      var text;
      text = Annotator.Util.escape(annotation.text || "");
      return $(field).html(this.convert(text));
    };
    /**
     * @param {?} text
     * @return {?}
     */
    Markdown.prototype.convert = function(text) {
      return this.converter.makeHtml(text);
    };
    return Markdown;
  }(Annotator.Plugin);
  Annotator.Plugin.Tags = function(_super) {
    /**
     * @return {?}
     */
    function Tags() {
      this.setAnnotationTags = __bind(this.setAnnotationTags, this);
      this.updateField = __bind(this.updateField, this);
      _ref4 = Tags.__super__.constructor.apply(this, arguments);
      return _ref4;
    }
    __extends(Tags, _super);
    Tags.prototype.options = {
      /**
       * @param {string} string
       * @return {?}
       */
      parseTags : function(string) {
        var tags;
        string = $.trim(string);
        /** @type {Array} */
        tags = [];
        if (string) {
          tags = string.split(/\s+/);
        }
        return tags;
      },
      /**
       * @param {?} array
       * @return {?}
       */
      stringifyTags : function(array) {
        return array.join(" ");
      }
    };
    /** @type {null} */
    Tags.prototype.field = null;
    /** @type {null} */
    Tags.prototype.input = null;
    /**
     * @return {?}
     */
    Tags.prototype.pluginInit = function() {
      if (!Annotator.supported()) {
        return;
      }
      this.field = this.annotator.editor.addField({
        label : Annotator._t("Add some tags here") + "\u2026",
        load : this.updateField,
        submit : this.setAnnotationTags
      });
      this.annotator.viewer.addField({
        load : this.updateViewer
      });
      if (this.annotator.plugins.Filter) {
        this.annotator.plugins.Filter.addFilter({
          label : Annotator._t("Tag"),
          property : "tags",
          /** @type {function (string, Array): ?} */
          isFiltered : Annotator.Plugin.Tags.filterCallback
        });
      }
      return this.input = $(this.field).find(":input");
    };
    /**
     * @param {string} string
     * @return {?}
     */
    Tags.prototype.parseTags = function(string) {
      return this.options.parseTags(string);
    };
    /**
     * @param {?} array
     * @return {?}
     */
    Tags.prototype.stringifyTags = function(array) {
      return this.options.stringifyTags(array);
    };
    /**
     * @param {Object} field
     * @param {Object} annotation
     * @return {?}
     */
    Tags.prototype.updateField = function(field, annotation) {
      var value;
      /** @type {string} */
      value = "";
      if (annotation.tags) {
        value = this.stringifyTags(annotation.tags);
      }
      return this.input.val(value);
    };
    /**
     * @param {?} field
     * @param {Object} annotation
     * @return {?}
     */
    Tags.prototype.setAnnotationTags = function(field, annotation) {
      return annotation.tags = this.parseTags(this.input.val());
    };
    /**
     * @param {Object} field
     * @param {Object} annotation
     * @return {?}
     */
    Tags.prototype.updateViewer = function(field, annotation) {
      field = $(field);
      if (annotation.tags && ($.isArray(annotation.tags) && annotation.tags.length)) {
        return field.addClass("annotator-tags").html(function() {
          var string;
          return string = $.map(annotation.tags, function(text) {
            return'<span class="annotator-tag">' + Annotator.Util.escape(text) + "</span>";
          }).join(" ");
        });
      } else {
        return field.remove();
      }
    };
    return Tags;
  }(Annotator.Plugin);
  /**
   * @param {string} input
   * @param {Array} tags
   * @return {?}
   */
  Annotator.Plugin.Tags.filterCallback = function(input, tags) {
    var node;
    var scripts;
    var completed;
    var tag;
    var i;
    var _i;
    var hasScripts;
    var _len;
    if (tags == null) {
      /** @type {Array} */
      tags = [];
    }
    /** @type {number} */
    completed = 0;
    /** @type {Array} */
    scripts = [];
    if (input) {
      scripts = input.split(/\s+/g);
      /** @type {number} */
      i = 0;
      hasScripts = scripts.length;
      for (;i < hasScripts;i++) {
        node = scripts[i];
        if (tags.length) {
          /** @type {number} */
          _i = 0;
          _len = tags.length;
          for (;_i < _len;_i++) {
            tag = tags[_i];
            if (tag.indexOf(node) !== -1) {
              completed += 1;
            }
          }
        }
      }
    }
    return completed === scripts.length;
  };
  /**
   * @param {Object} config
   * @param {Object} options
   * @return {?}
   */
  Annotator.prototype.setupPlugins = function(config, options) {
    var name;
    var copy;
    var pluginConfig;
    var plugins;
    var url;
    var win;
    var _i;
    var _len;
    var _results;
    if (config == null) {
      config = {};
    }
    if (options == null) {
      options = {};
    }
    win = Annotator.Util.getGlobal();
    /** @type {Array} */
    plugins = ["Unsupported", "Auth", "Tags", "Filter", "Store", "AnnotateItPermissions"];
    if (win.Showdown) {
      plugins.push("Markdown");
    }
    url = win.location.href.split(/#|\?/).shift() || "";
    pluginConfig = {
      Tags : {},
      Filter : {
        filters : [{
          label : Annotator._t("User"),
          property : "user"
        }, {
          label : Annotator._t("Tags"),
          property : "tags"
        }]
      },
      Auth : {
        tokenUrl : config.tokenUrl || "http://annotateit.org/api/token"
      },
      Store : {
        prefix : config.storeUrl || "http://annotateit.org/api",
        annotationData : {
          uri : url
        },
        loadFromSearch : {
          uri : url
        }
      }
    };
    for (name in options) {
      if (!__hasProp.call(options, name)) {
        continue;
      }
      copy = options[name];
      if (__indexOf.call(plugins, name) < 0) {
        plugins.push(name);
      }
    }
    $.extend(true, pluginConfig, options);
    /** @type {Array} */
    _results = [];
    /** @type {number} */
    _i = 0;
    /** @type {number} */
    _len = plugins.length;
    for (;_i < _len;_i++) {
      name = plugins[_i];
      if (!(name in pluginConfig) || pluginConfig[name]) {
        _results.push(this.addPlugin(name, pluginConfig[name]));
      } else {
        _results.push(void 0);
      }
    }
    return _results;
  };
}.call(this);
