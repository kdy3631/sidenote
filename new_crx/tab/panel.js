var _currentPageURL = null;
var _noteList = [];

$(document).ready(function() {
    initApp().then(function() {
        
        // 로딩 완료 되었다는 메시지 전달하기 ->background.js
        chrome.runtime.sendMessage({ msgType: 'sideNoteLoaded' });

        // note list 업데이트
        updateNoteList();
        
    });
});

//

$('#panel-refresh-btn').on('click',function(){
    location.href = location.href;

});


function initApp() {
    console.log('initApp()');

    // 이벤트 리스너 붙이기
    $('#sort-list').change(function() {
        console.log('sort onChanged');
        let newSort = $('#sort-list').val();
        _noteList = sortNoteList(_noteList, newSort);
        renderNoteList();
    });
    $('#panel-plus-btn').click(function() {

        var newData = {
            timestamp: moment(new Date()).format('YYYY-MM-DD hh:mm:ss'),
            title: '',
            url: _currentPageURL,
            memo: '',
            hashtag: [],
        };

        $('.new-editor-holder').empty();
        $('.new-editor-holder').append(buildEditorByNote(newData));
        
    });
    $('#panel-setting-btn').click(function() {

        console.log('open setting (google sheet id)');
        $('.setting-panel').toggleClass('hide-in');

    });

    //

    // 현재 페이지의 실제 URL 가져오기
    return new Promise(function(resolve) {
        chrome.runtime.sendMessage({
            msgType: 'getCurrentPage'
        }, function(url) {
            _currentPageURL = url || null;
            console.log('_currentPageURL=', _currentPageURL);
            resolve();
        });
    });
}

//

function updateNoteList() {
    console.log('updateNoteList()');
    getNotesByURL(_currentPageURL).then(function(noteList) {
        console.log('noteList', noteList);
        var sortType = $('#sort-list').val();
        _noteList = sortNoteList(noteList, sortType);
        renderNoteList();
    });
}

function renderNoteList() {
    var $listHolder = $('.note-item-wrapper');
    $listHolder.empty();
    for (var i=0; i<_noteList.length; i++) {
        var note = _noteList[i];
        var $noteElem = buildNoteElem(note);
        $listHolder.append($noteElem);
    }
}

function sortNoteList(noteList, sortType) {

    if ('latest' === sortType ) {
        return noteList.sort(function(a, b) {
            var a_timestamp = moment(a.timestamp, 'YYYY-MM-DD hh:mm:ss').toDate().getTime();
            var b_timestamp = moment(b.timestamp, 'YYYY-MM-DD hh:mm:ss').toDate().getTime();
            return a_timestamp - b_timestamp;
        });
    }

    if ('location' === sortType) {
        // TODO
        // ...
    }

    if ('alphabet' === sortType) {
        return noteList.sort(function(a, b) {
            return (''+a.title).localeCompare(b.title);
        });
    }

    return noteList;
}

function buildNoteElem(note) {
    var $noteElem = $('<div class="note-item">');
    
    // 1. heading
    var $head = $('<div class="panel-heading-btn">');
    $head.click(function() {
      $body.toggleClass('hide');
    });
    $head.append('<div class="panel-title">'+note.title+'</div>');

    // 1-1. heading btns
    var $headBtns = $('<div class="panel-heading-btns">');

    var $editBtn = $('<div class="edit-btn btn">Edit</div>');
    $editBtn.click(function(e) {
        console.log('onClickEdit', note.id);

        $noteElem.css('display', 'none');
        var $editor = $('<div class="note-editor panel-wrapper">');
        $editor.append(buildEditorByNote(note));
        $noteElem.after($editor);

        // header 까지 event 가 올라가서, body 가 열려버리는 것 방지
        e.stopPropagation();
    });
    $headBtns.append($editBtn);

    var $deleteBtn = $('<div class="delete-btn btn">');
    $deleteBtn.click(function(e) {
        console.log('onClickDelete', note.id);
        $noteElem.remove();
        deleteNote(note).then(function() {
            updateNoteList();
        });
        
        // header 까지 event 가 올라가서, body 가 열려버리는 것 방지
        e.stopPropagation();
    });
    $deleteBtn.append('<i class="xi-trash xi"></i>');
    $headBtns.append($deleteBtn);

    $head.append($headBtns);

    $noteElem.append($head);
  
    // 2. body
    var $body = $('<div class="panel-collapse-body hide">');
    $body.append('<div class="title-name timestamp" id="currentTime">'+note.timestamp+'</div>');
    $body.append('<div class="title-name url" id="currentTime">'+note.url+'</div>');
    $body.append('<div class="title-name memo" id="currentTime">'+note.memo+'</div>');
    // TODO : hashtags 는 이쁘게하기 (<div class="tag"> ...)
    $body.append('<div class="title-name" id="currentTime">'+note.hashtag+'</div>');
  
    $noteElem.append($body);
  
    return $noteElem;
}

function buildTagElem(hashTag) {
    let $hashTag = $('<div class="tag"></div>');
    $hashTag.text(hashTag);
    let $hashDelBtn = $('<i class="xi-close xi"></i>');
    $hashDelBtn.click(function(e) {
        $hashTag.remove();
    });
    $hashTag.append($hashDelBtn);
    return $hashTag;
}

function buildEditorByNote(note) {
    
    let $noteEditor = $('<div class="noteItem" id="note-'+note.id+'">');
    
    $noteEditor.append('<div class="timestamp" id="timestamp"></div>');
    
    $noteEditor.append('<div class="title-name">title</div>');
    
    let $titleInput = $('<textarea class="urlPage-title-input" id="note-input-title"></textarea>');
    $titleInput.val(note.title);
    $noteEditor.append($titleInput);

    $noteEditor.append('<div class="title-name" id ="note-memo">memo</div>');

    let $memoInput = $('<textarea class="comment-content" id="note-input-memo"></textarea>');
    $memoInput.val(note.memo);
    $noteEditor.append($memoInput);

    $noteEditor.append('<div class="title-name">hashtag</div>');

    let $hashTagInputWrapper = $('<div class="input-group input-group-sm collapse-hash">');
    let $hashTagInput = $('<input class="form-control ui-autocomplete-input keyword-input" type="text" autocomplete="off" placeholder="Enter to create keyword" style="min-width: 155px;" id="hashtag">');
    $hashTagInput.keydown(function(key) {
        if (key.keyCode == 13) {
            var hashTag = $hashTagInput.val();
            var $hashTag = buildTagElem(hashTag);
            $hashTagList.append($hashTag);
            $hashTagInput.val('');
        }
    });

    $hashTagInputWrapper.append($hashTagInput);
    $noteEditor.append($hashTagInputWrapper);

    let $hashTagList = $('<div class="tag-list"></div>');
    for (var i=0; i<note.hashtag.length; i++) {
        let hashTag = note.hashtag[i];
        let $hashTag = buildTagElem(hashTag);
        $hashTagList.append($hashTag);
    }
    $noteEditor.append($hashTagList);

    let $editorBtns = $('<div class="edit-btns">');
    let $saveBtn = $('<button type="button" class="btn btn-default save-btn" id="save-btn">Save</button>');
    $saveBtn.click(function() {

        note.timestamp = note.timestamp
        note.title = $titleInput.val();
        note.memo = $memoInput.val();

        let newHashTags = [];
        var $tags = $hashTagList.children();
        for (var i=0; i<$tags.length; i++) {
            var $tag = $($tags[i]);
            newHashTags.push($tag.text());
        }
        var hashTagValue = '';
        for (var i=0; i<newHashTags.length; i++) {
            var tag = newHashTags[i];
            hashTagValue += tag;
            if (i < (newHashTags.length-1)) {
                hashTagValue += ',';
            }
        }
        note.hashtag = hashTagValue;

        // 빠르게 저장하는 UX
        $noteEditor.remove();
        var tempData = JSON.parse(JSON.stringify(note));
        tempData['timestamp'] = moment(new Date(tempData['timestamp'])).format('YYYY-MM-DD hh:mm:ss');
        var hashTags = tempData['hashtag'].split(',');
        var parsedHashTags = [];
        for (var j=0; j<hashTags.length; j++) {
            var hashTag = hashTags[j];
            parsedHashTags.push(hashTag);
        }
        tempData['hashtag'] = parsedHashTags;
        _noteList.push(tempData);
        renderNoteList();
        
        // 실제 데이터는 구글 시트에 업데이트 요청하고, 완료시 다시 그려낸다
        updateNote(note).then(function() {
            updateNoteList();
        });

    });
    $editorBtns.append($saveBtn);

    let $cancelBtn = $('<button type="button" class="btn btn-default cancel-btn" id="cancel-btn">Cancel</button>');
    $cancelBtn.click(function() {
        $noteEditor.remove();
        renderNoteList();
    });
    $editorBtns.append($cancelBtn);

    $noteEditor.append($editorBtns);

    return $noteEditor;
}