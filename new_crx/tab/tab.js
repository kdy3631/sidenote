
// 메시지 리스너
chrome.runtime.onMessage.addListener(onMessageReceived);

// 전역변수
var _iframeLoaded = false;
var iframeOnLoadCallback = undefined;

//

function onMessageReceived(request, sender, sendResponse) {

    let method = request.method;
   
    if ('appendIframe' === method) appendIframe(sendResponse);
    if ('sideNoteLoaded' === method) {
        iframeOnLoaded();
      
    }
   
    if ('showSidebar' === method) $('iframe#kt-annotator-panel').removeClass('kt-annotator-hide');
    if ('hideSidebar' === method) $('iframe#kt-annotator-panel').addClass('kt-annotator-hide');
 
    if ('isSidebarOpen' === method) isSidebarOpen(sendResponse);
    if ('isJournalInjectedTab' === method) sendResponse({ data: true });

    return true;
}

function appendIframe(callback) {
    if (_iframeLoaded) {
        callback('already appended');
        return;
    } else {
        _iframeLoaded = true;

        $('html').remove('iframe#kt-annotator-panel');
        let iframe = $('<iframe id="kt-annotator-panel" '+
                               'class="kt-annotator-hide kt-annotator-collapse" '+
                               'src="'+chrome.extension.getURL('/tab/panel.html')+'"></iframe>');
        $('html').append(iframe);

        // 2중 스크롤 방지
        $('#kt-annotator-panel').mouseenter(function() {
            scrollTop = window.scrollY;
        });
        window.addEventListener('scroll', function() {
            let isHovered = $('iframe#kt-annotator-panel:hover').length > 0;
            if (isHovered) window.scrollTo(window.scrollX, scrollTop);
        });

        // iframe 내부 까지 로딩이 성공하면 callback 해준다.
        //(우클릭 해서 추가할 경우, 이벤트 허공에 이벤트 던지는것 방지)
        iframeOnLoadCallback = callback;
    }
}

function iframeOnLoaded() {
    if (iframeOnLoadCallback) {
        iframeOnLoadCallback('async onloaded');
        iframeOnLoadCallback = undefined;
    }
}

function isSidebarOpen(callback) {
    let isSidebarOpen = false;
    
    if (0 < $('iframe#kt-annotator-panel').length) {
        isSidebarOpen = !$('iframe#kt-annotator-panel').hasClass('kt-annotator-hide');
    }

    callback({ data: isSidebarOpen });
}

