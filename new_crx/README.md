journal crx 의 구조 중,
`background <-> single tab <-> iframe` 까지만 구현 되어 있는 코드입니다.
(이 프로젝트는 browserify 돌릴 필요 없습니다)

일반적인 코드 구조
```
// 코드 최상단에는 메소드/이벤트(메시지) 리스너가 맵핑만 되어있음
this.sayHello = doSayHello;
this.sayBye = doSayBye;

//

// 아래로는 위에서 맵핑한 function 들이 모여져 있음

function doSayHello() {
    console.log('hello');
    ...
}

function doSayBye() {
    console.log('bye);
    ...
}

...

```

background, tab, iframe 메시지 구현 방법

chrome.runtime.sendMessage -> background 라고 부르는 영역으로 메시지 전달
chrome.tabs.sendMessage(tabId, {}) -> background(chrome.runtime) 영역에서 특정 탭으로 메시지 전달


background 에서 특정 tab 의 side note 까지 메시지 전달 하려면...
```
1. background -> tab (bacgrkound 에서, 특정 tab의 id로 sendMessage)
2. tab -> iframe (tab 에서 메시지 리스너를 통해 메시지를 받고, 이것을 다시 iframe 에 sendMessage)
3. iframe 에서 메시지 리스너를 통해 메시지 받아서 처리
```

side note 에서 크롬 background 에 메세지 질의하여 응답 받기
```
1. iframe -> background (chrome.runtime.sendMessage 사용 하여 크롬 background 에 메시지 전달)
2. background -> tab (bacgrkound 에서, 다시 해당 tab의 id로 sendMessage) 
3. tab -> iframe
4. iframe 에서 리스너를 통해 메시지 받아서 처리
```
