// 앱 첫 설치 된 경우
chrome.runtime.onInstalled.addListener(onInstalledApp);

// 브라우저의 탭 에서 일어나는 이벤트들 관리
// https://developer.chrome.com/extensions/tabs
chrome.tabs.onCreated.addListener(onCreatedTab);
chrome.tabs.onActivated.addListener(onActivatedTab);
chrome.tabs.onUpdated.addListener(onUpdatedTab);

// 브라우저 윈도우 에서 일어나는 이벤트들 관리 (새탭이 아닌, 새창으로 아예 창이 다른 경우의 이벤트는 여기로 처리가 됨)
// https://developer.chrome.com/extensions/windows
chrome.windows.onFocusChanged.addListener(onFocusChangedTab);


//

// 익스텐션 아이콘 클릭 이벤트
chrome.browserAction.onClicked.addListener(onClickBrowserAction);

// 메시지 리스너
chrome.runtime.onMessage.addListener(onMessageReceived);

// 전역변수
var _injectJournal = false;

//

function onInstalledApp() {
    
    // 저장소 초기화 (저장소에는 2가지가 있으며, 편한것을 골라 쓰도록 합시다)
    // https://developer.chrome.com/apps/storage
    chrome.storage.local.clear();
    chrome.storage.sync.clear();

    // 초기 기본 값 세팅할 것 있으면 여기서 하는 것이 좋습니다
    // chrome.storage.sync.set({ 'googleSheetId': null });
    // ...
    
}

function onCreatedTab(tab) {
    tabChanged(tab);
}
function onActivatedTab(event) {
    let tabId = event.tabId;
    chrome.tabs.get(tabId, function(tab) {
        if (tab) tabChanged(tab);
    });
}
function onUpdatedTab(tabId, event, tab) {
    let needUpdate = false;

    if (event.status && 'complete' === event.status) needUpdate = true;
    if (event.title) needUpdate = true;

    if (needUpdate) tabChanged(tab);
}
function onFocusChangedTab() {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tabs) {
        if (0 < tabs.length) {
            var tab = tabs[0];
            tabChanged(tab);
        }
    });
}

// 탭 변화 경우의 수가 다양하기 때문에, tabChanged function 에서 일괄 처리한다
function tabChanged(tab) {
    
    // icon change
    if (isNeedDisableTab(tab)) {
        chrome.browserAction.disable(tab.id);
        updateIcon(tab.id, 'disable');
    } else {
        chrome.browserAction.enable(tab.id);
        updateIcon(tab.id, 'idle');
    }

}

// 사용 불가한 탭(사용 불가한 사이트) 인지 확인
function isNeedDisableTab(tab) {
    let blockedUrls = ['chrome://', 'https://chrome.google.com/webstore/'];
    let tabUrl = tab.url;
    let isDisabledTab = false;

    for (let i=0; i<blockedUrls.length; i++) {
        let blockedUrl = blockedUrls[i];
        if (-1 < tabUrl.indexOf(blockedUrl)) {
            isDisabledTab = true;
        }
    }

    return isDisabledTab;
}

function updateIcon(tabId, type) {
    let path = '/img/icon16.png';

    if ('disable' === type) path = '/img/icon-disabled.png';
    if ('idle' === type) path = '/img/icon16.png';
    if ('active' === type) path = '/img/icon16.png';

    chrome.browserAction.setIcon({
        path: path,
        tabId: tabId
    });
}

function onClickBrowserAction(tab) {
    chrome.tabs.sendMessage(tab.id, {
        method: 'isSidebarOpen'
    }, function(response) {
        let isOpen = false;
        if (response && response.data) isOpen = true;
        
        if (isOpen) {
            chrome.tabs.sendMessage(tab.id, { method: 'hideSidebar' });
            
        } else {
            chrome.tabs.sendMessage(tab.id, {
                method: 'isJournalInjectedTab'
            }, function(response) {
                var isInjected = false;
                if (response && response.data) isInjected = true;

                if (isInjected) {
                    chrome.tabs.sendMessage(tab.id, { method: 'showSidebar' });
                } else {
                    
                    if (!_injectJournal) {
                        _injectJournal = true;

                        chrome.tabs.insertCSS(tab.id, { file: 'background/css/crx-iframe.css' });
                        chrome.tabs.executeScript(tab.id, { file: 'lib/jquery-2.1.4.min.js' }, function() {

                            chrome.tabs.executeScript(tab.id, { file: 'tab/tab.js' }, function() {
                                chrome.tabs.sendMessage(tab.id, {
                                    method: 'appendIframe'
                                }, function(resonse) {
                                    
                                    // iframe 내부까지 로딩 완료
                                    // ...

                                });
                                _injectJournal = false;
                                chrome.tabs.sendMessage(tab.id, { method: 'showSidebar' });
                            });

                        });

                    }

                }

            });
        }

    });
}



function onMessageReceived(msg, sender, callback) {
    console.log('onMessageReceived', msg, sender);

    let method = msg.msgType;
    let tab = sender.tab;

  

    if ('hideSidebar' === method) {
        chrome.tabs.sendMessage(tab.id, { method: 'hideSidebar' });
    }
  
    if('getCurrentPage' === method) {
        console.log('getCurrentPage', sender.tab.url);
        let tab = sender.tab;
        callback(tab.url);
    }

    if ('sideNoteLoaded' === method) {
        chrome.tabs.sendMessage(tab.id, { method: 'sideNoteLoaded' });

    }

    if('showInputBox' === method) {
        chrome.tabs.sendMessage(tab.id,{method:'showInputBox'});
    }

    if('createNote' === method) {
        chrome.tabs.sendMessage(tab.id,{method:'createNote'});
    }

    if('cancelInputText' === method) {
        chrome.tabs.sendMessage(tab.id,{method:'cancelInputText'});
    }

    if('toggleBody' === method) {

        chrome.tabs.sendMessage(tab.id,{method:'toggleBody'});
    }

    if('deleteNote' ===method) {
        chrome.tabs.sendMessage(tab.id,{method:'deleteNote'});

    }
    //enterKey
    if('enterKey' ===method) {
        chrome.tabs.sendMessage(tab.id,{method:'enterKey'});

    }
    // if ('updateBadgeIcon' === method) updateBadgeIcon(tab);

    return true;
}

function getCurrentPage(tab, callback) {
    let data = {
        title: tab.title,
        url: tab.url
    };
    callback(data);
}
