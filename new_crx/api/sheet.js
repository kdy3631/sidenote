 //todo : 12.3 내 url로 다시 고치기
 
 function getNotesByURL(url) {
    console.log('getNotesByURL', url);
    // TODO TEST
    // url = 'https://docs.google.com/spreadsheets/d/11sKvu_sTrCVg4-LF0KtML6yoyIVHm7WxUW0zYMZIvh8/edit#gid=0C';
    /////
    return new Promise(function(resolve, reject) {
        $.ajax({
            url: 'https://script.google.com/macros/s/AKfycbxYo71Zi8_z713FcEXLsTuz-zIvh8MWCSxRZe8E0satMUMawkkD/exec?url='+url,
            type: 'GET',
            dataType: 'json',
            success : function(dataList) {
                var parsedDataList = [];
                for (var i=0; i<dataList.length; i++) {
                    var data = dataList[i];
                    data['timestamp'] = moment(new Date(data['timestamp'])).format('YYYY-MM-DD hh:mm:ss');
                    var hashTags = data['hashtag'].split(',');
                    var parsedHashTags = [];
                    for (var j=0; j<hashTags.length; j++) {
                        var hashTag = hashTags[j];
                        parsedHashTags.push(hashTag);
                    }
                    data['hashtag'] = parsedHashTags;
                    parsedDataList.push(data);
                }
                resolve(parsedDataList);
            },
            fail: reject,
        });
    });
}

function updateNote(note) {
    console.log('updateNote', note);
    return new Promise(function(resolve, reject) {
        $.ajax({
            url: 'https://script.google.com/macros/s/AKfycbxYo71Zi8_z713FcEXLsTuz-zIvh8MWCSxRZe8E0satMUMawkkD/exec',
            type: 'POST',
            dataType: 'json',
            data: note,
            complete: function() {
                resolve();
            },
        });
    });
}

function deleteNote(note) {
    console.log('deleteNoteAjax', note);
    return new Promise(function(resolve, reject) {
        $.ajax({
            url: 'https://script.google.com/macros/s/AKfycbxYo71Zi8_z713FcEXLsTuz-zIvh8MWCSxRZe8E0satMUMawkkD/exec?action=del',
            type: 'POST',
            dataType: 'json',
            data: note,
            complete: function() {
                resolve();
            },
        });
    });
  }